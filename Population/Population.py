__author__ = 'Miguel A. Marcos Perez'
from Genome.Genome import Genome
# Now is used cPickle for optimization
# from copy import deepcopy as copy

# Pickle import
try:
    import cPickle as pickle
except:
    import pickle

from Commons.error import inputParameterError

class Population(object):
    # Population class is a container of Genomes

    def __init__(self, genomeList):
        if not isinstance(genomeList, list):
            if not isinstance(genomeList, Genome):
                raise inputParameterError("Params: Input type is " + str(type(genomeList)) + " instead of list")
            else:
                # If input is a Genome convert to list
                genomeList = [genomeList]
        self.__genomeList = genomeList

    def __eq__(self, other):
        if isinstance(other, Population):
            return self.__genomeList == other.__genomeList
        else:
            return False

    # Get method
    #   This will return the list or the Genome in the position i

    def get(self, i=None):
        if i is None:
            # return copy(self.__genomeList)
            return pickle.loads(pickle.dumps(self.__genomeList, -1))
        # return copy(self.__genomeList[i])
        return pickle.loads(pickle.dumps(self.__genomeList[i], -1))

    # SET method:
    #   If the input is a list we will set the list of Genomes
    #   If i is not an integer and the input is a Genome we will remplace the i Genome
    def set(self, Input, integer=None):
        if integer is None:
            if isinstance(Input, list):
                self.__genomeList = Input
        elif isinstance(Input, Genome):
            self.__genomeList[integer] = Input

    def size(self):
        return len(self.__genomeList)

    # ADD method:
    #   This will add a Genome to the list or append the input list to our existing list
    def add(self, Input):
        if isinstance(Input, Genome):
            self.__genomeList.append(Input)
        elif isinstance(Input, list):
            self.__genomeList.append(Input)

    def copy(self):
        return pickle.loads(pickle.dumps(self, -1))

    def __str__(self):
        string = ""
        num = 0
        for ind in self.__genomeList:
            string += str(ind)
            if ind != self.__genomeList[-1]:
                string += ", "
            num += 1
            if num >= 10:
                string += ",..."
                break
        return string
