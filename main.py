__author__ = 'Miguel A Marcos Perez'

import configparser
import sys
import random
import time
import importlib

from Population.Population import Population
from Commons.error import inputParameterError

'''
    This code is for read the conf file and configure the GA for diferent type of executions
'''


def __main__():
    # If arguments are null default file
    if len(sys.argv) == 1:
        configFile = "cfg/f7NoTrans.ini"
    else:
        configFile = sys.argv[1]

    '''
    Parser cfg file
    '''

    def initParser():
        global seed, algorithm, strmaxEvals, problem, debugTxt, mutStr, pMut, croStr, pCross, selStr, terStr, terPConv
        global iniStr, gen, popType, popMinVal, popMaxVal, genSize, selNGroup, eliStr, staStr, Config, genType

        print("Configuration file: " + configFile)

        Config = configparser.ConfigParser()
        Config.read(configFile)

        print(Config.sections())

        # GA
        seed = int(Config.get("Algorithm", "seed"))
        print("\t[Algorithm] seed: " + str(seed))
        random.seed(seed)

        strmaxEvals = Config.get("Algorithm", "maxEvals")
        if strmaxEvals == "None":
            strmaxEvals = None
        else:
            strmaxEvals = int(strmaxEvals)
        print("\t[Algorithm] maxEvals: " + str(strmaxEvals))

        algorithm = Config.get("Algorithm", "type")
        print("\t[Algorithm] type: " + algorithm)
        problem = Config.get("Algorithm", "problem")
        print("\t[Algorithm] problem: " + problem)

        # Debug mode?
        debugTxt = Config.get("Algorithm", "debug")
        print("\t[Algorithm] debug: " + debugTxt)
        if debugTxt:
            global debug
            debug = True

        # Mutator
        mutStr = Config.get("Mutator", "mutatorType")
        pMut = float(Config.get("Mutator", "pMut"))
        if pMut > 1 or pMut < 0:
            raise inputParameterError()
        print("\t[Mutator] pMut: " + str(pMut))
        print("\t[Mutator] type: " + str(mutStr))

        # Crossover
        croStr = Config.get("Crossover", "crossoverType")
        pCross = float(Config.get("Crossover", "pCross"))
        if pCross > 1 or pCross < 0:
            raise inputParameterError()
        print("\t[Crossover] pCross: " + str(pCross))
        print("\t[Crossover] type: " + str(croStr))

        # Selector
        selStr = Config.get("Selector", "selectorType")
        selNGroup = int(Config.get("Selector", "selectorNGroup"))
        print("\t[Selector] type: " + str(selStr))
        print("\t[Selector] nGroup: " + str(selNGroup))

        # Elitism
        eliStr = Config.get("Elitism", "elitismType")
        print("\t[Elitism] type: " + str(eliStr))

        # Stats
        staStr = Config.get("Stats", "statsType")
        print("\t[Stats] type: " + str(staStr))

        # Terminator
        terStr = Config.get("Terminator", "terminatorType")
        print("\t[Terminator] type: " + str(terStr))
        terPConv = float(Config.get("Terminator", "variable"))
        print("\t[Terminator] value: " + str(terPConv))

        # Initializer
        iniStr = Config.get("Initializer", "initializerType")
        print("\t[Initializer] type: " + str(iniStr))

        # Genome
        gen = Config.get("Algorithm", "genomeType")
        print("\t[Genome] type: " + str(gen))
        genType = Config.get("Algorithm", "valueType")
        print("\t[Genome] value type: " + str(genType))

        # Pop loader
        popType = Config.get("Algorithm", "initialPopType")
        popMinVal = float(Config.get("Algorithm", "popMinVal"))
        popMaxVal = float(Config.get("Algorithm", "popMaxVal"))
        genSize = int(Config.get("Algorithm", "genSize"))

    '''
    Import all objects
    '''

    # Import problem
    # For this functionality we have to load a class dinamically. This library can help to load a class.
    # To work, the class have to be named as the file

    # This code get the attr of a file that is in the config file
    # Later we instanciate a new object

    def initImports():
        global pop, mut, cross, sel, pro, eli, ter, sta, ini, genomeClassObj

        # Problem loader
        problemClassObj = getattr(importlib.import_module("Problems." + problem), problem)
        pro = problemClassObj()

        # Import Mutator
        mutatorClassObj = getattr(importlib.import_module("Operators.Mutator"), mutStr)
        mut = mutatorClassObj(pMut)

        # Import Crossover
        crossoverClassObj = getattr(importlib.import_module("Operators.Crossover"), croStr)
        cross = crossoverClassObj(pCross)

        # Import Selector
        selectorClassObj = getattr(importlib.import_module("Operators.Selector"), selStr)
        sel = selectorClassObj(selNGroup)

        # Import Elitism
        elitismClassObj = getattr(importlib.import_module("Operators.Elitism"), eliStr)
        eli = elitismClassObj()

        # Import Elitism
        statsClassObj = getattr(importlib.import_module("Operators.Stats"), staStr)
        sta = statsClassObj()

        # Import Terminator
        terminatorClassObj = getattr(importlib.import_module("Operators.Terminator"), terStr)
        ter = terminatorClassObj(terPConv, pro.optCriterion())

        # Import Initializer
        initializerClassObj = getattr(importlib.import_module("Operators.Initializer"), iniStr)
        ini = initializerClassObj()

        # Genome loader
        genomeClassObj = getattr(importlib.import_module("Genome." + gen), gen)

    '''
    Allele converter
    '''

    def initAllele():
        genAlleleTmp = Config.get("Algorithm", "allelePop")
        # Get allele in list or tuple
        genAlleleList = genAlleleTmp.split(";")
        if len(genAlleleList) == 1:
            # Create only one tuple
            genAlleleTmp = (float(genAlleleTmp.split(",")[0]), float(genAlleleTmp.split(",")[1]))
        else:
            if len(genAlleleList) != genSize:
                raise inputParameterError("Params: Input param allelePop is not genSize length")
            genAlleleTmp = []
            for allele in genAlleleList:
                genAlleleTmp.append((float(allele.split(",")[0]), float(allele.split(",")[1])))

        return genAlleleTmp

    '''
    Init population
    '''

    def initPop():
        popLst = []
        global pop, mut, cross, sel, pro, eli, ter, sta, ini, genomeClassObj
        # If pop is random
        popSize = Config.get("Algorithm", "popSize")
        if popType == "RANDOM":
            if int(popSize) == 1:
                popLst = genomeClassObj([random.uniform(popMinVal, popMaxVal) for x in range(genSize)],
                                        genAllele, genType)
                popLst = ini(popLst)
            else:
                for i in range(0, int(popSize)):
                    popLstTmp = genomeClassObj([random.uniform(popMinVal, popMaxVal) for x in range(genSize)],
                                               genAllele, genType)
                    popLstTmp = ini(popLstTmp)
                    popLst.append(popLstTmp)
        # If pop is in conf file
        elif popType == "SET":
            '''
            popStr = Config.get("Algorithm", "initialPop")
            indLst = popStr.split(";")
            for ind in indLst:
                indStr = ind.split(",")
                indInt = [int(i) for i in indStr]
                popLst.append(genomeClassObj(indInt, genAllele, genType))
            '''
            print("POP SET option is disabled, only RANDOM is enabled")
            sys.exit(-1)
        # if pop is in conf file
        elif popType == "ZERO":
            '''
            for i in range(0, int(popSize)):
                popLstTmp = genomeClassObj([0.0 for x in range(genSize)], genAllele, genType)
                popLst.append(popLstTmp)
            '''
            print("POP ZERO option is disabled, only RANDOM is enabled")
            sys.exit(-1)

        return Population(popLst)

    initParser()
    initImports()
    genAllele = initAllele()
    print("\t[Allele]: " + str(genAllele))
    pop = initPop()
    print("\t[POP] type: " + popType)
    print("\t[POP] initialPop: " + str(pop))
    print("\t[POP] size: " + str(pop.size()))

    '''
    Init algorithm
    '''
    # GA object created
    algorithmClassObj = getattr(importlib.import_module("Algorithm." + algorithm), algorithm)
    if algorithm == "GA":
        alg = algorithmClassObj(pop, mut, cross, sel, pro, eli, ter, sta, {})
    elif algorithm == "DE":
        F=1
        alg = algorithmClassObj(pop, mut.getPMut(), cross, pro, sta, ter, F, {})


    start_time = time.time()
    nEvals, popFin = alg.evolve(maxEvals=strmaxEvals)
    elapsed_time = time.time() - start_time

    '''
    Extra info
    '''

    print("_____________________________________")
    print("Evolve ended in " + str(elapsed_time) + " seconds.")
    popSorted = alg.sort(popFin)
    print("Final Population: " + str(popSorted))
    best = alg.best(popFin)
    print("Best of: " + str(best))
    print("number of generations: " + str(sta.getNGen()))
    print("Number of total evals: " + str(nEvals))

    # Class Stat info
    print("-------Stats class info-------")
    print("Number of Crossovers: " + str(sta.getCross()))
    print("Number of Mutations: " + str(sta.getMut()))
    print("Last best scores: " + str(sta.getBest()))


__main__()
