__author__ = 'Miguel A Marcos Perez'

import configparser
import sys
import random
import time
import importlib

from Population.Population import Population
from Genealogy.Genealogy import Genealogy
from Operators.Stats import GAStatistics
from Commons.error import inputParameterError

from Mos.MOSEA2 import MOSEA2

'''
    This code is for read the conf file and configure the GA for diferent type of executions
'''


def __main__():
    # If arguments are null default file
    if len(sys.argv) == 1:
        configFile = "cfg/cfgMosTest.ini"
    else:
        configFile = sys.argv[1]

    Config = configparser.ConfigParser()
    Config.read(configFile)

    print(Config.sections())

    strAlgorithmList, pop, strmaxEvals, strSteps, algorithmList, problem, pro = initParser(configFile, Config)

    # Here we split two ways depend on number of algorithms
    if len(algorithmList) == 1:
        alg = algorithmList[0]
        start_time = time.time()
        nEvals, popFin = alg.evolve(strmaxEvals, pop)
        elapsed_time = time.time() - start_time
    else:
        print("->Participation<-")
        # Mos participation
        participation = Config.get("Participation", "type")
        PartMinPart = float(Config.get("Participation", "minPart"))
        ParRestoreGen = float(Config.get("Participation", "restoreGen"))
        PartPfData = float(Config.get("Participation", "pfData"))
        PartAdjust = float(Config.get("Participation", "adjust"))
        print("\t[Participation] type: " + participation)
        print("\t[Participation] \tminPart: " + str(PartMinPart))
        print("\t[Participation] \trestoreGen: " + str(ParRestoreGen))
        print("\t[Participation] \tpfData: " + str(PartPfData))
        print("\t[Participation] \tAdjust: " + str(PartAdjust))
        participationFunction = getattr(importlib.import_module("Mos.MOSParticipationFunction"), participation)

        if PartAdjust is not None:
            participationObject = participationFunction(PartMinPart, ParRestoreGen, PartPfData, PartAdjust)
        else:
            participationObject = participationFunction(PartMinPart, ParRestoreGen, PartPfData)

        # Mos quality
        print("->Quality<-")
        quality = Config.get("Quality", "type")
        comparator = Config.get("Quality", "comparator")
        print("\t[Quality] type: " + quality)
        print("\t[Quality] \tcomparator: " + comparator)
        qualityFunction = getattr(importlib.import_module("Mos.MOSQualityFunction"), quality)
        comparatorFunction = getattr(importlib.import_module("Operators.Comparator"), comparator)
        comparatorObject = comparatorFunction()
        qualityObject = qualityFunction(comparatorObject)

        if not qualityObject.needsGenealogy():
            genealogy = Genealogy()
            genealogy.setNecesary(False)

        print("->Creating MOS<-")

        from Mos.MOSTechniqueSet import MOSTechniqueSet

        mosTechiqueSet = MOSTechniqueSet(int(strSteps), qualityObject, participationObject, pro)
        for alg in algorithmList:
            mosTechiqueSet.registerAlgorithm(alg)

        # We have to create MOSEA2
        mosea = MOSEA2(pop)
        # mosea.setDebug()

        print("[MOS] Number of algorithms: " + str(mosTechiqueSet.nAlgorithm()))

        start_time = time.time()
        # Here we have to evolve algorithms
        # nEvals, popFin, MosalgorithmList = mosTechiqueSet.evolve(pop, strmaxEvals)
        nEvals, popFin, MosalgorithmList = mosea.step(strmaxEvals, pop)
        elapsed_time = time.time() - start_time

    '''
    Extra info
    '''

    print("_____________________________________")
    print("Evolve ended in " + str(elapsed_time) + " seconds.")
    popSorted = alg.sort(popFin)
    best = alg.best(popFin)
    if not pop.get(1).size() > 10:
        print("Final Population: " + str(popSorted))
        print("Best of: " + str(best))
    print("\tScore: " + str(alg.evaluateGenome(best)))

    # Print Stats
    if len(algorithmList) == 1:
        stats = alg.getStats()
        staCros = stats.getCross()
        staMut = stats.getMut()
        gens = stats.getNGen()
        evals = stats.getEvals()
        print("{: >5} ({:10.3}s)-> gens: {: >5} - evals {: >10} - quality {:10.5} - participation {:.5}".
              format(strAlgorithmList[0], elapsed_time, gens, evals, 0.0, 1.0))
    else:
        staCros, staMut, staGen, staEvals, timeLst = mosTechiqueSet.getStats()
        print("Number of generations by algorithm: ")

        for algN, gens, evals, tmpTime, mosAlg in zip(strAlgorithmList, staGen, staEvals, timeLst, MosalgorithmList):
            print("{: >5} ({:10.3}s)-> gens: {: >5} - evals {: >10} - quality {:10.5} - participation {:.5}".
                  format(algN, tmpTime, gens, evals, mosAlg.getQuality(), mosAlg.getRatio()))

    print("\n\tNumber of total evals: " + str(nEvals))

    # Class Stat info
    print("\n\n-------Stats class info-------")
    print("Number of Crossovers: " + str(staCros))
    print("Number of Mutations: " + str(staMut))
    # print("Last best scores: " + str(sta.getBest()))

    # Output file
    outputFile = Config.getboolean("Generic", "outputFile")
    outputFileName = Config.get("Generic", "outputFileName")
    if outputFile:
        if outputFileName == "None":
            fileName = time.time()
            file = open("logs/"+str(int(fileName))+".txt", 'w')
        else:
            file = open("logs/"+str(outputFileName), 'w')
        arrayPop = popSorted.get()
        file.write("Problem " + problem+ "\n")
        file.write("Score: " + str(alg.evaluateGenome(best)) + "\n")
        for ind in arrayPop:
            file.write(str(ind)+"\n")


'''
Parser cfg file
'''


def initParser(configFile, Config):
    print("Configuration file: " + configFile)

    seed = int(Config.get("Generic", "seed"))
    debugTxt = Config.get("Generic", "debug")
    strmaxEvals = Config.get("Generic", "maxEvals")
    strSteps = Config.get("Generic", "steps")
    problem = Config.get("Generic", "problem")

    algorithmList = []

    # SEED
    random.seed(seed)

    # MAXEVALS
    if strmaxEvals == "None":
        strmaxEvals = None
    else:
        strmaxEvals = int(strmaxEvals)

    # DEBUG
    if debugTxt:
        global debug
        debug = True

    print("\t[Generic] debug: " + debugTxt)
    print("\t[Generic] seed: " + str(seed))
    print("\t[Generic] maxEvals: " + str(strmaxEvals))
    print("\t[Generic] steps: " + str(strSteps))
    print("\t[Generic] problem: " + problem)

    problemClassObj = getattr(importlib.import_module("Problems." + problem), problem)
    pro = problemClassObj()

    # Here we have to create population <--
    GAgen = Config.get("Generic", "genomeType")
    GAgenType = Config.get("Generic", "valueType")
    GAgenSize = int(Config.get("Generic", "genSize"))
    GAgenAlleleTmp = Config.get("Generic", "allelePop")
    GApopSize = Config.get("Generic", "popSize")
    GAgenAlleleList = GAgenAlleleTmp.split(";")

    print("\t[Population] Genome type: " + str(GAgen))
    print("\t[Population] \ttype: " + str(GAgenType))
    print("\t[Population] \tsize: " + str(GApopSize))
    print("\t[Population] \tsize of gen: " + str(GAgenSize))
    print("\t[Population] \trange: " + str(GAgenAlleleList))

    GAgenomeClassObj = getattr(importlib.import_module("Genome." + GAgen), GAgen)

    popLst = []
    if len(GAgenAlleleList) == 1:
        # Create only one tuple
        if GAgenType == "int":
            genAlleleTmp = (float(GAgenAlleleTmp.split(",")[0]), float(GAgenAlleleTmp.split(",")[1]))
            popLstTmp = [
                GAgenomeClassObj([(random.uniform(genAlleleTmp[0], genAlleleTmp[1])) for x in range(GAgenSize)],
                                 genAlleleTmp, GAgenType) for j in range(int(GApopSize))]
        else:
            genAlleleTmp = (float(GAgenAlleleTmp.split(",")[0]), float(GAgenAlleleTmp.split(",")[1]))
            popLstTmp = [GAgenomeClassObj([random.uniform(genAlleleTmp[0], genAlleleTmp[1]) for x in range(GAgenSize)],
                                          genAlleleTmp, GAgenType) for j in range(int(GApopSize))]
        popLst = popLstTmp
    else:
        if len(GAgenAlleleList) != GAgenSize:
            raise inputParameterError("Params: Input param allelePop is not genSize length")
        for i in range(0, int(GApopSize)):
            genAlleleTmp = []
            indLstTmp = []
            for allele in GAgenAlleleList:
                genAlleleTmp.append((float(allele.split(",")[0]), float(allele.split(",")[1])))
                indLstTmp.append(random.uniform(float(allele.split(",")[0]), float(allele.split(",")[1])))
            popLstTmp = GAgenomeClassObj(indLstTmp, genAlleleTmp, GAgenType)
            popLst.append(popLstTmp)

    pop = Population(popLst)

    # Genealogy
    genealogy = Genealogy()
    genealogy.initRoot(popLst)

    # Here we have to iterate over algorithms
    strAlgorithmList = Config.get("Generic", "algorithmList")
    print("[Generic] Algorithm list: " + str(strAlgorithmList))
    strAlgorithmList = strAlgorithmList.replace(" ", "")
    strAlgorithmList = strAlgorithmList.split(",")

    for alg in strAlgorithmList:
        if not Config.has_section(alg):
            raise inputParameterError("[ERROR] The algorithm " + str(alg) + " is not defined in the cfg file")

        # Now we have to load variables for each algorithm
        algorithm = Config.get(alg, "algorithm")
        if algorithm == "GA":
            # Mutator
            GAmutStr = Config.get(alg, "mutatorType")
            GApMut = float(Config.get(alg, "pMut"))
            if GApMut > 1 or GApMut < 0:
                raise inputParameterError()

            # Crossover
            GAcroStr = Config.get(alg, "crossoverType")
            GApCross = float(Config.get(alg, "pCross"))
            if GApCross > 1 or GApCross < 0:
                raise inputParameterError()

            # Selector
            GAselStr = Config.get(alg, "selectorType")
            GAselNGroup = int(Config.get(alg, "selectorNGroup"))

            # Elitism
            GAeliStr = Config.get(alg, "elitismType")

            # Stats
            GAStatisticsGA = GAStatistics()

            # Terminator
            GAterStr = Config.get(alg, "terminatorType")
            GAterPConv = float(Config.get(alg, "terminatorVar"))

            # Initializer
            GAiniStr = Config.get(alg, "initializerType")

            print("->Parse " + alg + " algorithm<-")

            print("\t[" + alg + "] Algorithm type: " + algorithm)
            print("\t[" + alg + "] Mutator type: " + str(GAmutStr))
            print("\t[" + alg + "] \tpMut: " + str(GApMut))
            print("\t[" + alg + "] Crossover type: " + str(GAcroStr))
            print("\t[" + alg + "] \tpCross: " + str(GApCross))
            print("\t[" + alg + "] Selector type: " + str(GAselStr))
            print("\t[" + alg + "] \tnGroup: " + str(GAselNGroup))
            print("\t[" + alg + "] Elitism type: " + str(GAeliStr))
            print("\t[" + alg + "] Terminator type: " + str(GAterStr))
            print("\t[" + alg + "] \tvar: " + str(GAterPConv))
            print("\t[" + alg + "] Initializer type: " + str(GAiniStr))

            mutatorClassObj = getattr(importlib.import_module("Operators.Mutator"), GAmutStr)
            crossoverClassObj = getattr(importlib.import_module("Operators.Crossover"), GAcroStr)
            selectorClassObj = getattr(importlib.import_module("Operators.Selector"), GAselStr)
            elitismClassObj = getattr(importlib.import_module("Operators.Elitism"), GAeliStr)
            terminatorClassObj = getattr(importlib.import_module("Operators.Terminator"), GAterStr)
            initializerClassObj = getattr(importlib.import_module("Operators.Initializer"), GAiniStr)

            GAmut = mutatorClassObj(GApMut)
            GAcross = crossoverClassObj(GApCross)
            GAsel = selectorClassObj(GAselNGroup)
            GAeli = elitismClassObj()
            GAter = terminatorClassObj(GAterPConv, pro.optCriterion())
            GAini = initializerClassObj()

            # Here we create the algorithm
            GAalgorithmClassObj = getattr(importlib.import_module("Algorithm." + algorithm), algorithm)
            GAalg = GAalgorithmClassObj(algorithm, Population([]), GAmut, GAcross, GAsel, pro, GAeli, GAter,
                                        GAStatisticsGA, {})
            algorithmList.append(GAalg)

        elif algorithm == "DE":
            # Mutator
            DEpMut = float(Config.get(alg, "pMut"))
            if DEpMut > 1 or DEpMut < 0:
                raise inputParameterError()

            # Crossover
            DEcroStr = Config.get(alg, "crossoverType")
            DEpCross = float(Config.get(alg, "pCross"))
            if DEpCross > 1 or DEpCross < 0:
                raise inputParameterError()

            DEFStr = Config.get(alg, "F")
            GAStatisticsDE = GAStatistics()

            # Terminator
            DEterStr = Config.get(alg, "terminatorType")
            DEterPConv = float(Config.get(alg, "terminatorVar"))

            # Initializer
            DEiniStr = Config.get(alg, "initializerType")

            print("->Parse " + alg + " algorithm<-")

            print("\t[" + alg + "] Algorithm type: " + algorithm)
            print("\t[" + alg + "] \tpMut: " + str(DEpMut))
            print("\t[" + alg + "] Crossover type: " + str(DEcroStr))
            print("\t[" + alg + "] \tpCross: " + str(DEpCross))
            print("\t[" + alg + "] Terminator type: " + str(DEterStr))
            print("\t[" + alg + "] \tvar: " + str(DEterPConv))
            print("\t[" + alg + "] Initializer type: " + str(DEiniStr))

            crossoverClassObj = getattr(importlib.import_module("Operators.Crossover"), DEcroStr)
            terminatorClassObj = getattr(importlib.import_module("Operators.Terminator"), DEterStr)
            initializerClassObj = getattr(importlib.import_module("Operators.Initializer"), DEiniStr)

            DEcross = crossoverClassObj(DEpCross)
            DEter = terminatorClassObj(DEterPConv, pro.optCriterion())
            DEini = initializerClassObj()

            # Here we create the algorithm
            DEalgorithmClassObj = getattr(importlib.import_module("Algorithm." + algorithm), algorithm)
            F = int(DEFStr)
            DEalg = DEalgorithmClassObj(algorithm, Population([]), DEpMut, DEcross, pro, GAStatisticsDE, DEter, F, {})
            algorithmList.append(DEalg)

        elif algorithm == "LS":
            # Stats
            GAStatisticsLS = GAStatistics()

            # Terminator
            LSterStr = Config.get(alg, "terminatorType")
            LSlsStr = Config.get(alg, "ls")
            LSterPConv = float(Config.getfloat(alg, "terminatorVar"))

            # MTSOperators
            if Config.has_option(alg, "MTSAdjustFailed"):
                adjustFail = Config.getfloat(alg, "MTSAdjustFailed")
            else:
                adjustFail = 0.5
            if Config.has_option(alg, "MTSAdjustMin"):
                adjustMin = Config.getfloat(alg, "MTSAdjustMin")
            else:
                adjustMin = 0.4
            if Config.has_option(alg, "MTSMoveLeft"):
                moveLeft = Config.getfloat(alg, "MTSMoveLeft")
            else:
                moveLeft = 1.0
            if Config.has_option(alg, "MTSMoveRight"):
                moveRight = Config.getfloat(alg, "MTSMoveRight")
            else:
                moveRight = 0.5

            print("->Parse " + alg + " algorithm<-")

            print("\t[" + alg + "] Algorithm type: " + algorithm)
            print("\t[" + alg + "] Local Search type: " + LSlsStr)
            print("\t[" + alg + "] Terminator type: " + str(LSterStr))
            print("\t[" + alg + "] \tvar: " + str(LSterPConv))

            lsClassObj = getattr(importlib.import_module("Operators.LocalSearch"), LSlsStr)
            terminatorClassObj = getattr(importlib.import_module("Operators.Terminator"), LSterStr)

            LSls = lsClassObj(adjustFail, adjustMin, moveLeft, moveRight)
            LSter = terminatorClassObj(LSterPConv, pro.optCriterion())

            # Here we create the algorithm
            LSalgorithmClassObj = getattr(importlib.import_module("Algorithm." + algorithm), algorithm)
            LSalg = LSalgorithmClassObj(algorithm, pop, LSls, pro, LSter, GAStatisticsLS, {})
            algorithmList.append(LSalg)

    return strAlgorithmList, pop, strmaxEvals, strSteps, algorithmList, problem, pro


__main__()
