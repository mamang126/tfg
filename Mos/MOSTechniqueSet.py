__author__ = 'Miguel A Marcos'

from Algorithm.Algorithm import Algorithm
from Population.Population import Population
from Problems.Problem import Problem

from Mos.MOSQualityFunction import MOSQuality
from Mos.MOSParticipationFunction import MOSParticipation

from random import uniform, randint
from Commons.error import inputParameterError
from operator import attrgetter
import time

'''
    This class will support a list of algorithms like MOSTechniqueSet in C code.
    In this class i will implement all type of methods for modify the internal list of algorithms
'''

class MOSTechniqueSet(object):

    __instance = None
    isInit = False

    # singleton
    def __new__(cls, *args, **kwargs):
        if MOSTechniqueSet.__instance is None:
            MOSTechniqueSet.__instance = object.__new__(cls)
        return MOSTechniqueSet.__instance

    @staticmethod
    def __handle__():
        return MOSTechniqueSet.__instance

    # Constructor
    def __init__(self, steps, QualityFunction, ParticipationFunction, problem):
        if not MOSTechniqueSet.isInit:
            if not isinstance(QualityFunction, MOSQuality):
                raise inputParameterError("Params: Input type is " + str(type(QualityFunction)) + " instead of QualityFunction")
            if not isinstance(ParticipationFunction, MOSParticipation):
                raise inputParameterError("Params: Input type is " + str(type(ParticipationFunction)) + " instead of ParticipationFunction")
            if not isinstance(problem, Problem):
                raise inputParameterError("Params: Input type is " + str(type(problem)) + " instead of Problem")

            self.__operation = problem.optCriterion()
            self.__AlgorithmList = []
            self.steps = steps
            self.gen = 0
            self.quality = QualityFunction
            self.participation = ParticipationFunction
            MOSTechniqueSet.isInit = True

    def getGens(self):
        return self.gen

    def addGens(self):
        self.gen += 1

    def registerAlgorithm(self, alg, ratio=0.0):
        if not isinstance(alg, Algorithm):
            raise inputParameterError("Parametro: Input type is " + str(type(alg)) + " instead of Algorithm")

        self.__AlgorithmList.append(MosAlgorithm(alg, ratio))

    def unRegisterAlgorithm(self, pos):
        if not isinstance(pos, int):
            raise inputParameterError("Parametro: Input type is " + str(type(pos)) + " instead of Integer")

        self.__AlgorithmList.pop(pos)

    def getAlgorithm(self, pos):
        if not isinstance(pos, int):
            raise inputParameterError("Parametro: Input type is " + str(type(pos)) + " instead of Integer")

        return self.__AlgorithmList[pos]

    def getAllAlgorithm(self):
        return self.__AlgorithmList

    def nAlgorithm(self):
        return len(self.__AlgorithmList)

    def setPartRatio(self, pos, ratio):
        if not isinstance(pos, int):
            raise inputParameterError("Parametro: Input type is " + str(type(pos)) + " instead of Integer")
        if not isinstance(ratio, float):
            raise inputParameterError("Parametro: Input type is " + str(type(ratio)) + " instead of Float")

        self.__AlgorithmList[pos].setRatio(ratio)

    def getPartRatio(self, pos=None):
        if pos is None:
            ret = []
            for alg in self.__AlgorithmList:
                ret.append(alg.getRatio())
            return ret
        else:
            if not isinstance(pos, int):
                raise inputParameterError("Parametro: Input type is " + str(type(pos)) + " instead of Integer")

            return self.__AlgorithmList[pos].getRatio()

    def sumPartRatios(self):
        ac = 0
        for ele in self.__AlgorithmList:
            ac += ele.getRatio()

        return ac

    def resetQuality(self):
        for ele in self.__AlgorithmList:
            ele.resetQuality()

    def getQuality(self, pos=None):
        if pos is None:
            ret = []
            for alg in self.__AlgorithmList:
                ret.append(alg.getQuality())
            return ret
        else:
            if not isinstance(pos, int):
                raise inputParameterError("Parametro: Input type is " + str(type(pos)) + " instead of Integer")
            return self.__AlgorithmList[pos].getQuality()

    def setQuality(self, pos, qua):
        if not isinstance(pos, int):
            raise inputParameterError("Parametro: Input type is " + str(type(pos)) + " instead of Integer")
        if not isinstance(qua, float):
            raise inputParameterError("Parametro: Input type is " + str(type(qua)) + " instead of Float")

        self.__AlgorithmList[pos].setQuality(qua)

    def initPartRatios(self):
        ratio = 1.0 / len(self.__AlgorithmList)
        for ele in self.__AlgorithmList:
            ele.setRatio(ratio)

    def getBestAlgorithms(self):
        out = []
        if self.__operation == "MAX":
            best = max(self.__AlgorithmList, key=attrgetter('_MosAlgorithm__Quality'))
        elif self.__operation == "MIN":
            best = min(self.__AlgorithmList, key=attrgetter('_MosAlgorithm__Quality'))

        i = 0
        for alg in self.__AlgorithmList:
            if alg.getQuality() == best.getQuality():
                out.append(i)
            i += 1
        return out

    def getStats(self):
        staCross = 0
        staMut = 0
        staGen = []
        staEvals = []
        timeLst = []
        for alg in self.__AlgorithmList:
            stats = alg.getAlgorithm().getStats()

            tmpStaCross = stats.getCross()
            tmpStaMut = stats.getMut()
            tmpStaGen = stats.getNGen()
            tmpEvalsGen = stats.getEvals()
            tmpTime = stats.getTime()

            staCross += tmpStaCross
            staMut += tmpStaMut
            staGen.append(tmpStaGen)
            staEvals.append(tmpEvalsGen)
            timeLst.append(tmpTime)
        return staCross, staMut, staGen, staEvals, timeLst

    def getRandomAlgorithm(self):
        # Here we calculate one random number
        ran = uniform(0.0, 1.0)
        if not self.sumPartRatios() == 1.0:
            raise Exception("[ERROR] PArtRatio sum is not 1.0: " + str(self.sumPartRatios()))
        sumx = 0
        idx = 0
        for ratios in self.__AlgorithmList:
            tmpRatio = ratios.getRatio()
            sumx += tmpRatio
            if ran <= sumx:
                break
            idx += 1
        return idx

    def reset(self):
        self.__AlgorithmList = []
        self.gen = 0

    def assignPartRatio(self, maxEvals):
        evalsList = []

        maxEvalsStep = int(maxEvals / self.steps)

        for ratios in self.__AlgorithmList:
            evalsList.append(int(maxEvalsStep * ratios.getRatio()))

        sumT = 0
        for eva in evalsList:
            sumT += eva

        if sumT != maxEvalsStep:
            for i in range(0, maxEvalsStep-sumT):
                ran = randint(0, len(self.__AlgorithmList) - 1)
                evalsList[ran] += 1

        return evalsList

    def evolve(self, pop, maxEvals=None):
        if not isinstance(pop, Population):
            raise inputParameterError("Params: Input type is " + str(type(pop)) + " instead of Population")
        if not isinstance(maxEvals, int):
            if not maxEvals is None:
                raise inputParameterError("Params: Input type is " + str(type(maxEvals)) + " instead of Integer")
            else:
                maxEvals = pop.size()

        popSize = pop.size()

        self.initPartRatios()

        evals = 0
        for step in range(0, self.steps):
            i = 0

            # Here we assign each eval to a alg updated inside loop
            listRatios = self.assignPartRatio(maxEvals)
            listEvals = []

            for alg in self.__AlgorithmList:
                # For each algorithm
                start_time = time.time()
                evalsUsed, finalPop = alg.getAlgorithm().evolve(listRatios[i], pop)
                elapsed_time = time.time() - start_time

                alg.getAlgorithm().getStats().setTime(elapsed_time)
                listEvals.append(evalsUsed)

                i += 1
                pop = finalPop
                evals += evalsUsed

                if evals % popSize == 0:
                    # Add 1 gen
                    self.addGens()

                # Update Quality
                # TODO: newInds??
                qua = self.quality.update(pop, alg.getAlgorithm().ID, 0)

                # reset genealogy and save
                alg.setQuality(float(qua))

            # Update partRatio
            self.participation.update(self)

            # Prints for information
            best = self.getBestAlgorithms()
            print(str("\n{:-^100}").format("Step:" + str(step)))
            for x in range(0, self.nAlgorithm()):
                if best.count(x)==1:
                    print(str(self.getAlgorithm(x).getAlgorithm().ID) +
                        " {:->10} - P: {:<20} - Q: {:<20} - E: {:<10} BEST".
                          format(self.getGens(), self.getPartRatio(x), self.getQuality(x), listEvals[x]))
                else:
                    print(str(self.getAlgorithm(x).getAlgorithm().ID) +
                        " {:>10} - P: {:<20} - Q: {:<20} - E: {:<10}".
                          format(self.getGens(), self.getPartRatio(x), self.getQuality(x), listEvals[x]))

        return evals, pop, self.__AlgorithmList
        
# This is an internal class that store an Algorithm and the Ratio.
class MosAlgorithm:
    def __init__(self, alg, ratio=0.0, qua=0.0):
        if not isinstance(alg, Algorithm):
            raise inputParameterError("Parametro: Input type is " + str(type(alg)) + " instead of Algorithm")
        if not isinstance(ratio, float):
            raise inputParameterError("Parametro: Input type is " + str(type(ratio)) + " instead of Float")
        if not isinstance(qua, float):
            raise inputParameterError("Parametro: Input type is " + str(type(qua)) + " instead of Float")
        self.__Algoritm = alg
        self.__Ratio = ratio
        self.__Quality = qua

    def getAlgorithm(self):
        return self.__Algoritm

    def getRatio(self):
        return self.__Ratio

    def setAlgorithm(self, alg):
        if not isinstance(alg, Algorithm):
            raise inputParameterError("Parametro: Input type is " + str(type(alg)) + " instead of Algorithm")
        self.__Algoritm = alg

    def setRatio(self, ratio):
        if not isinstance(ratio, float):
            raise inputParameterError("Parametro: Input type is " + str(type(ratio)) + " instead of Float")
        self.__Ratio = ratio

    def resetQuality(self):
        self.__Quality = 0.0

    def getQuality(self):
        return self.__Quality

    def setQuality(self, qua):
        if not isinstance(qua, float):
            raise inputParameterError("Parametro: Input type is " + str(type(qua)) + " instead of Float")

        self.__Quality = qua
