__author__ = 'Miguel A. Marcos'

from Algorithm.Algorithm import Algorithm
from Population.Population import Population
from Commons.error import inputParameterError

from Mos.MOSTechniqueSet import MOSTechniqueSet

import time

class MOSEA2(Algorithm):

    def __init__(self, pop):
        super(MOSEA2, self).__init__(pop)
        self.__debugMode = False

    def setDebug(self):
        self.__debugMode = True

    def step(self, maxEvals, pop=None):
        if not isinstance(pop, Population):
            raise inputParameterError("Params: Input type is " + str(type(pop)) + " instead of Population")
        if not isinstance(maxEvals, int):
            if not maxEvals is None:
                raise inputParameterError("Params: Input type is " + str(type(maxEvals)) + " instead of Integer")
            else:
                maxEvals = pop.size()

        # Get MosTechniqueSet
        techSet = MOSTechniqueSet.__handle__()

        popSize = pop.size()

        techSet.initPartRatios()

        evals = 0
        for step in range(0, techSet.steps):
            i = 0

            # Here we assign each eval to a alg updated inside loop
            listRatios = techSet.assignPartRatio(maxEvals)
            listEvals = []
            listTime = []
            algorithms = techSet.getAllAlgorithm()

            for alg in algorithms:
                # For each algorithm
                start_time = time.time()
                evalsUsed, finalPop = alg.getAlgorithm().evolve(listRatios[i], pop)
                elapsed_time = time.time() - start_time

                alg.getAlgorithm().getStats().setTime(elapsed_time)
                listEvals.append(evalsUsed)
                listTime.append(elapsed_time)

                i += 1
                pop = finalPop
                evals += evalsUsed

                if evals % popSize == 0:
                    # Add 1 gen
                    techSet.addGens()

                # Update Quality
                # TODO: newInds??
                qua = techSet.quality.update(pop, alg.getAlgorithm().ID, 0)

                # reset genealogy and save
                alg.setQuality(float(qua))

            # Update partRatio
            techSet.participation.update(techSet)

            # Prints for information if debug
            if self.__debugMode:
                best = techSet.getBestAlgorithms()
                print(str("\n{:-^100}").format("Step:" + str(step)))
                for x in range(0, techSet.nAlgorithm()):
                    if best.count(x)==1:
                        print(str(techSet.getAlgorithm(x).getAlgorithm().ID)+"(" + str(listTime[x]) + ")" +
                            " {:->10} - P: {:<20} - Q: {:<20} - E: {:<10} BEST".
                              format(techSet.getGens(), techSet.getPartRatio(x), techSet.getQuality(x), listEvals[x]))
                    else:
                        print(str(techSet.getAlgorithm(x).getAlgorithm().ID) +"(" + str(listTime[x]) + ")" +
                            " {:>10} - P: {:<20} - Q: {:<20} - E: {:<10}".
                              format(techSet.getGens(), techSet.getPartRatio(x), techSet.getQuality(x), listEvals[x]))

        return evals, pop, techSet.getAllAlgorithm()
