from __future__ import absolute_import
__author__ = 'Miguel A. Marcos Perez'

#from Mos.MOSTechniqueSet import MOSTechniqueSet
from Commons.error import inputParameterError

class MOSParticipation(object):

    def __init__(self, minPart, restoreGen, pfData):
        self.__minPart = minPart
        self.__restoreGen = restoreGen
        self.__pfData = pfData

    def update(self, alg):
        pass

class ConstantParticipation(MOSParticipation):

    def __init__(self, minPart, restoreGen, pfData):
        super(ConstantParticipation, self).__init__(minPart, restoreGen, pfData)

    # This method will updatePartRatios like is done in MosTechniqueSet
    def update(self, techSet):
        if not(isinstance(techSet, MOSTechniqueSet)):
            raise inputParameterError("Params: Input type is " + str(type(techSet)) + " instead of MosTechSet")
        techSet.initPartRatios()

class DynamicParticipation(MOSParticipation):

    def __init__(self, minPart, restoreGen, pfData, adjust):
        self.__base = 0.0

        if adjust < 0.0 or adjust > 1.0:
            adjust = 0.5
        self.__adjust = adjust
        self.__minPart = minPart
        self.__restoreGen = restoreGen
        self.__pfData = pfData

        super(DynamicParticipation, self).__init__(minPart, restoreGen, pfData)

    def setBaseQual(self, base):
        self.__base = base

    def update(self, techSet):
        #if not(isinstance(techSet, MOSTechniqueSet)):
        #    raise inputParameterError("Params: Input type is " + str(type(techSet)) + " instead of MosTechSet")

        currGen = techSet.getGens()
        nTechs = techSet.nAlgorithm()

        if self.__restoreGen != 0 and currGen % self.__restoreGen == 0:
            techSet.initPartRatios()
            return None

        bestTechsPos = techSet.getBestAlgorithms()
        lenBest = len(bestTechsPos)
        if lenBest == 0 or lenBest == nTechs:
            return None

        bestQuality = techSet.getAlgorithm(bestTechsPos[0]).getQuality()

        # Here we obtain all tech in the set
        listTech = techSet.getAllAlgorithm()
        for i in range(0, len(listTech)):
            # If is not one of the best algorithms
            if bestTechsPos.count(i) == 0:
                try:
                    diff = abs((bestQuality - listTech[i].getQuality()) / (bestQuality - self.__base))
                except:
                    diff = abs((bestQuality - listTech[i].getQuality()))

                currentPart = techSet.getPartRatio(i)
                ratioInc = currentPart * self.__adjust * diff

                ratioInc = ratioInc / 100

                if currentPart - ratioInc > self.__minPart:
                    sharedRatio = ratioInc / lenBest

                    for j in range(0, lenBest):
                        currentBest = techSet.getPartRatio(bestTechsPos[j])
                        techSet.setPartRatio(bestTechsPos[j], currentBest + sharedRatio)
                    techSet.setPartRatio(i, currentPart - ratioInc)

                elif currentPart > self.__minPart:
                    sharedRatio = currentPart - self.__minPart / lenBest

                    for j in range(0, lenBest):
                        currentBest = techSet.getPartRatio(bestTechsPos[j])
                        techSet.setPartRatio(bestTechsPos[j], currentBest + sharedRatio)

                    techSet.setPartRatio(i, self.__minPart)

        # Correct possibly wrong values due to floating operations
        for i in range(0, len(listTech)):
            currentPart = techSet.getPartRatio(i)

            if currentPart < 0.0:
                techSet.setPartRatio(i, 0.0)
            elif currentPart > 1.0:
                techSet.setPartRatio(i, 1.0)
