__author__ = "Miguel A: Marcos Perez"

import random
from Commons.error import inputParameterError

def flip_coin(pMut):
    if pMut <= 0:
        pMut = 0.0
    elif pMut >= 1:
        pMut = 1.0

    if not isinstance(pMut, float):
        raise inputParameterError("Input parameters to the call are invalid", "Param: Input type is " +
                                          str(type(pMut)) + " instead of float")

    num = random.triangular(0, 1)
    return pMut > num
