__author__ = 'Miguel A. Marcos Perez'

import logging

tipoError = { 0:"IN_OUT", 1:"FUNCTION_IN", 2:"CLASS_IN"}

class inputParameterError(Exception):
    def __init__(self, desc=""):
        self.value = "Input parameters are invalid"
        self.desc = desc

    def __str__(self):
        return self.value + "\n >>" + self.desc

class arraySameLenError(Exception):
    def __init__(self, errorStr, desc=""):
        self.value = "Lengths of both arrays are not equals"
        self.desc = desc

    def __str__(self):
        return self.value + "\n >>" + self.desc
