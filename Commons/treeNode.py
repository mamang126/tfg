from Commons.error import inputParameterError


class Node:
    def __init__(self, val, parents=None, sons=None):
        if parents is not None:
            if not isinstance(parents, list):
                raise inputParameterError("Parametro: Input type is " + str(type(parents)) + " instead of list")
        if sons is not None:
            if not isinstance(sons, list):
                raise inputParameterError("Parametro: Input type is " + str(type(sons)) + " instead of list")

        self.value = val
        self.sons = sons
        self.parents = parents

    def getNumberChilds(self):
        return len(self.sons)

    def getNumberParents(self):
        return len(self.parents)

    def hasLeftChild(self):
        return self.leftChild

    def hasRightChild(self):
        return self.rightChild

    def isLeftChild(self):
        return self.parent and self.parent.leftChild == self

    def isRightChild(self):
        return self.parent and self.parent.rightChild == self

    def isRoot(self):
        return not self.parent

    def isLeaf(self):
        return not (self.rightChild or self.leftChild)

    def hasAnyChildren(self):
        return self.rightChild or self.leftChild

    def hasBothChildren(self):
        return self.rightChild and self.leftChild

    def replaceNodeData(self, value, lc, rc):
        self.value = value
        self.leftChild = lc
        self.rightChild = rc
        if self.hasLeftChild():
            self.leftChild.parent = self
        if self.hasRightChild():
            self.rightChild.parent = self


class Genealogy:
    def __init__(self, rootVal):
        root = Node(rootVal)
        self.root = root
        self.size = 1

    def length(self):
        return self.size

    def __len__(self):
        return self.size

    def addSons(self, valSon1, valSon2):
        # We have to go to the leaf from the left using findMin()
        leaf = self.__findMin()
        leaf.leftChild = Node(valSon1, leaf)
        leaf.rightChild = Node(valSon2, leaf)
        self.size += 2

    def getFather(self, nIter=1):
        # 0 return self node
        # Got to the left leaf and iter parent
        leaf = self.__findMin()
        parent = leaf
        for x in range(nIter):
            if parent.isRoot():
                raise Exception("Root node")
                return parent
            parent = parent.parent
        return parent

    def __findMin(self):
        current = self.root
        while current.hasLeftChild():
            current = current.leftChild
        return current
