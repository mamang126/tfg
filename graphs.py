__author__ = 'Miguel A Marcos Perez'

import random

import gc

from Genome.GA1DArrayGenome import GA1DArrayGenome

from Algorithm.GA import GA
from Algorithm.LS import LS
from Algorithm.DE import DE

from Population.Population import Population

from time import time

from Operators.Crossover import OnePointCrossover, BinomialCrossover
from Operators.Elitism import PopElitism
from Operators.Selector import TournamentSelector
from Operators.Mutator import FlipMutator
from Operators.Terminator import terminateUponGeneration
from Operators.Stats import GAStatistics
from Operators.LocalSearch import MTS_LS1

from Mos.MOSTechniqueSet import MOSTechniqueSet
from Mos.MOSParticipationFunction import DynamicParticipation
from Mos.MOSQualityFunction import AverageFitnessQuality
from Mos.MOSEA2 import MOSEA2

from Problems.Schwefel_Problem import Schewefel_Problem
from Problems.Shifted_Ackley import Shifted_Ackley
from Problems.Shifted_Rastrigin import Shifted_Rastrigin
from Problems.Shifted_Rosenbrock import Shifted_Rosenbrock
from Problems.Shifted_Griewank import Shifted_Griewank
from Problems.Shifted_Sphere import Shifted_Sphere
from Problems.f9 import f9
from Problems.f7 import f7
from Problems.f8 import f8
from Problems.f10 import f10
from Problems.f11 import f11

from Commons.error import inputParameterError

def generatePop(GApopSize, GAgenSize, allele):
    GAgenomeClassObj = GA1DArrayGenome
    GAgenType = "float"

    popLst = []
    for i in range(0, int(GApopSize)):
        indLstTmp = [random.uniform(allele[0], allele[1]) for x in range(GAgenSize)]

        popLstTmp = GAgenomeClassObj(indLstTmp, (allele[0], allele[1]), GAgenType)
        popLst.append(popLstTmp)

    population = Population(popLst)
    return population

def log(string):
    print(string)
    file.write(string+"\n")

#@profile
if __name__ == '__main__':
    # Enable garbage collector
    gc.enable()

    # Log file
    file = open("logs/"+str(time())+"_graph.txt", 'w')

    # Global variables
    pMut = 0.1
    pCross = 0.6
    F = 1
    steps = 10
    GAgenSize = 20
    GApopSize = 15
    seed = 101

    random.seed(seed)

    # List of problems
    problemList = []
    problemList.append((Schewefel_Problem(), -450, (-100, 100)))
    problemList.append((Shifted_Ackley(), -140, (-32, 32)))
    problemList.append((Shifted_Rastrigin(), -330, (-5, 5)))
    problemList.append((Shifted_Rosenbrock(), 390, (-100, 100)))
    problemList.append((Shifted_Griewank(), -180, (-600, 600)))
    problemList.append((Shifted_Sphere(), -450, (-100, 100)))
    problemList.append((f9(), 0, (-100, 100)))
    problemList.append((f8(), 0, (-65.535, 65.535)))
    problemList.append((f10(), 0, (-15, 15)))
    problemList.append((f7(), 0, (-10, 10)))
    problemList.append((f11(), 0, (-100, 100)))

    causistic = [25, 50]

    # Restart execution
    problemToStartBool = False
    problemToStart = 5
    algorithmToStartBool = False

    # test vars
    GAtest = True
    DEtest = True
    LStest = True
    MOStest = True

    log("List of problem len: " + str(len(problemList)))

    for cau in causistic:
        it = 1
        actPro = 0
        maxEvals = 5000 * cau
        GAgenSize = cau
        log("Test suite parameters: GenSize: " + str(GAgenSize) + ", maxEvals: " +
              str(maxEvals))

        for problemItem in problemList:
            # Resume execution
            if problemToStartBool and actPro != problemToStart:
                # next
                actPro += 1
                it += 1
                continue
            elif actPro == problemToStart:
                problemToStartBool = False

            problem = problemItem[0]
            optimum = problemItem[1]
            allele = problemItem[2]
            log("Problem " + problem.__class__.__name__ + ": " + str(it) + "/" + str(len(problemList)))

            pro = problem

            # GA only.............................................................................
            # pop init
            if GAtest:
                pop = generatePop(GApopSize, GAgenSize, allele)
                mut = FlipMutator(pMut)
                cros = OnePointCrossover(pCross)
                sel = TournamentSelector()
                eli = PopElitism()
                terGA = terminateUponGeneration(100, "MAX")
                staGA = GAStatistics()
                GAalgorithm = GA('GA', pop, mut, cros, sel, pro, eli, terGA, staGA)

                startTime = time()

                # Start coroutine
                evals, popFinal = GAalgorithm.evolve(maxEvals)

                elapsedTime = time() - startTime
                best = GAalgorithm.best(popFinal).getScore()
                log("GA " + " T: {:<20}, B: {:<20}, O: {:<5}, E: {:<10}".format(elapsedTime, best, optimum, optimum-best))
            else:
                print("GA Skip")

            # DE only.............................................................................
            # pop init
            if DEtest:
                pop = generatePop(GApopSize, GAgenSize, allele)
                staDE = GAStatistics()
                cros = BinomialCrossover(pCross)
                terDE = terminateUponGeneration(100, "MAX")
                DEalgorithm = DE('DE', pop, pMut, cros, pro, staDE, terDE, F)

                startTime = time()

                evals, popFinal = DEalgorithm.evolve(maxEvals)

                elapsedTime = time() - startTime
                best = DEalgorithm.best(popFinal).getScore()
                log("DE " + " T: {:<20}, B: {:<20}, O: {:<5}, E: {:<10}".format(elapsedTime, best, optimum, optimum-best))
            else:
                print("DE Skip")

            # LS only.............................................................................
            # pop init
            if LStest:
                pop = generatePop(GApopSize, GAgenSize, allele)
                staLS = GAStatistics()
                mts_ls1 = MTS_LS1()
                terLS = terminateUponGeneration(100, "MAX")
                LSalgorithm = LS('LS', pop, mts_ls1, pro, terLS, staLS)

                startTime = time()

                evals, popFinal = LSalgorithm.evolve(maxEvals)

                elapsedTime = time() - startTime
                best = LSalgorithm.best(popFinal).getScore()
                log("LS " + " T: {:<20}, B: {:<20}, O: {:<5}, E: {:<10}".format(elapsedTime, best, optimum, optimum-best))
            else:
                print("LS Skip")

            # MOS    .............................................................................
            # pop init
            if MOStest:
                pop = generatePop(GApopSize, GAgenSize, allele)

                # Here we have to create all MOS classes
                # GA<-------------
                mut = FlipMutator(pMut)
                cros = OnePointCrossover(pCross)
                sel = TournamentSelector()
                pro = problem
                eli = PopElitism()
                ter = terminateUponGeneration(100, "MAX")
                staGA = GAStatistics()
                GAalgorithm = GA('GA', pop, mut, cros, sel, pro, eli, ter, staGA)

                # DE<---------------
                staDE = GAStatistics()
                cros = BinomialCrossover(pCross)
                DEalgorithm = DE('DE', pop, pMut, cros, pro, staDE, ter, F)

                # LS<---------------
                staLS = GAStatistics()
                mts_ls1 = MTS_LS1()
                LSalgorithm = LS('LS', pop, mts_ls1, pro, ter, staLS)

                # MOS<--------------
                quality = AverageFitnessQuality()
                participation = DynamicParticipation(0.05, 0, 0, 0.05)
                techSet = MOSTechniqueSet(steps, quality, participation, pro)

                techSet.registerAlgorithm(GAalgorithm)
                techSet.registerAlgorithm(DEalgorithm)
                techSet.registerAlgorithm(LSalgorithm)

                GAalgorithm.evaluatePopulation(pop)
                old_best = GAalgorithm.best(pop).getScore()

                mosea = MOSEA2(pop)
                startTime = time()

                evals, finalPop = mosea.evolve(maxEvals, pop)

                best = GAalgorithm.best(finalPop).getScore()
                elapsedTime = time() - startTime
                it += 1
                log("MOS T: {:<20}, B: {:<20}, O: {:<5}, E: {:<10}".format(elapsedTime, best, optimum, optimum-best))
                # Reset MOS
                techSet.reset()

                actPro += 1
            if algorithmToStartBool:
                GAtest = True
                DEtest = True
                LStest = True
                MOStest = True

