__author__ = 'Miguel A. Marcos Perez'

from Genome.Genome import Genome
from Commons.error import inputParameterError
import random

class Initializer(object):

    def __init__(self):
        pass

    def __call__(self, *args, **kwargs):
        pass

class RealUniformInitializer(Initializer):

    def __call__(self, genome, genSize=None, rangeTuple=None):
        if not isinstance(genome, Genome):
            raise inputParameterError("Params: Input type is " + str(type(genome)) + " instead of Genome")

        if len(genome.getArray()) == 0:
            if genSize is None:
                raise inputParameterError("Params: Input needs genSize param")
            if rangeTuple is None:
                raise inputParameterError("Params: Input needs rangeTuple param")

            if not isinstance(genSize, int):
                raise inputParameterError("Params: Input type is " + str(type(genSize)) + " instead of Int")
            if not isinstance(rangeTuple, tuple):
                raise inputParameterError("Params: Input type is " + str(type(rangeTuple)) + " instead of Tuple")

            genome.setArray([0 for x in range(genSize)])
            genome.setRange(rangeTuple)

        minVal = genome.getAllelle()[0]
        maxVal = genome.getAllelle()[1]
        genArray = genome.getArray()
        if genSize is None:
            genSize = len(genArray)

        val = [random.uniform(minVal, maxVal) for x in range(genSize)]
        genome.setArray(val)

        return genome
