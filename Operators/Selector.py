__author__ = 'Miguel A. Marcos Perez'
import random
from operator import attrgetter
from Population.Population import Population

from Commons.error import inputParameterError

class Selector(object):
    def __init__(self):
        pass

    def __call__(self):
        pass

class TournamentSelector(Selector):

    def __init__(self, nGroup=10):
        self.__nGroup = nGroup
        pass

    def __call__(self, pop):
        if not isinstance(pop, Population):
            raise inputParameterError(str(type(pop)) + " instead of Population")

        # This is the implementation of the algorithm of Tournament Selector
        # TODO: The number of members is necesary?
        nGroup = self.__nGroup
        # 1) Obtenemos una lista de n Genomas de pop
        #       Calculamos n numeros aleatorios
        newList = []
        tamPop = pop.size()
        for n in range(nGroup):
            lstPop = pop.get(random.randint(0, tamPop-1))
            newList.append(lstPop)

        # 2) Elegimos al mejor de listaPosible y lo devolvemos
        return max(newList, key=attrgetter('_Genome__score'))
