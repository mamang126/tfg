from operator import attrgetter

__author__ = 'Miguel A. Marcos Perez'

from Population.Population import Population
from Commons.error import inputParameterError

class Elitism(object):
    def __call__(self, oldPopParents, oldPopChilds, opt):
        pass

    def selector(self):
        pass

class PopElitism(Elitism):

    def __init__(self):
        pass

    def __call__(self, oldPopParents, oldPopChilds, opt="MAX"):
        if not isinstance(oldPopChilds, Population):
            raise inputParameterError("Params: Input type is " + str(type(oldPopChilds)) + " instead of Population")
        if not isinstance(oldPopParents, Population):
            raise inputParameterError("Params: Input type is " + str(type(oldPopParents)) + " instead of Population")

        # We have to save the length of the pop
        tamPop = oldPopChilds.size()

        # We obtain all ind of this generation
        generation = list(oldPopParents.get() + oldPopChilds.get())

        # We have to sort these members
        generation.sort(key=attrgetter('_Genome__score'), reverse=opt == "MAX")

        return Population(generation[:tamPop])

class DEElitism(Elitism):

    def __init__(self):
        pass

    def __call__(self, oldPopChilds, oldPopParents, opt):
        if not isinstance(oldPopChilds, Population):
            raise inputParameterError("Params: Input type is " + str(type(oldPopChilds)) + " instead of Population")
        if not isinstance(oldPopParents, Population):
            raise inputParameterError("Params: Input type is " + str(type(oldPopParents)) + " instead of Population")

        parentsArray = oldPopParents.get()
        childsArray = oldPopChilds.get()
        if len(parentsArray) != len(childsArray):
            raise inputParameterError("Params: Population have not same len")

        resultArray = []

        for indNew, indOld in zip(childsArray, parentsArray):
            if opt == 'MAX':
                if indNew.getScore() >= indOld.getScore():
                    resultArray.append(indNew)
                else:
                    resultArray.append(indOld)
            else:
                if indNew.getScore() <= indOld.getScore():
                    resultArray.append(indNew)
                else:
                    resultArray.append(indOld)

        return Population(resultArray)

class LSElitism(Elitism):

    def __init__(self):
        pass

    def __call__(self, oldPopParents, oldPopChilds, opt="MAX"):
        if not isinstance(oldPopChilds, Population):
            raise inputParameterError("Params: Input type is " + str(type(oldPopChilds)) + " instead of Population")
        if not isinstance(oldPopParents, Population):
            raise inputParameterError("Params: Input type is " + str(type(oldPopParents)) + " instead of Population")

        oldPopParents.set(oldPopChilds.get(0), 0)

        return Population(oldPopParents)
