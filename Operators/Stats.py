__author__ = 'Miguel A Marcos Perez'

class Stats(object):

    # Class that contains methods to obtain multiple values of a population
    def addCross(self, n):
        pass

    def getCross(self):
        pass

    def addMut(self, n):
        pass

    def setTime(self, n):
        pass

    def getTime(self):
        pass

    def getMut(self):
        pass

    def addNGen(self, n):
        pass

    def addEvals(self, n):
        pass

    def setEvals(self, n):
        pass

    def getNGen(self):
        pass

    def addBest(self, best):
        pass

    def getBest(self):
        pass


class GAStatistics(Stats):

    def __init__(self):
        self.nCross = 0
        self.nMut = 0
        self.nGens = 0
        self.nEvals = 0
        self.time = 0.0
        self.listBest = []

    # Times called CrossOver
    def addCross(self, n):
        self.nCross += n

    def getCross(self):
        return self.nCross

    # Times called Mutator
    def addMut(self, n):
        self.nMut += n

    def getMut(self):
        return  self.nMut

    # Evals called
    def addEvals(self, n):
        self.nEvals += n

    def getEvals(self):
        return self.nEvals

    def setTime(self, n):
        self.time += n

    def getTime(self):
        return self.time

    # Number of generations
    def addNGen(self, n):
        self.nGens += n

    def getNGen(self):
        return self.nGens

    # List of best of every gen
    def addBest(self, best):
        self.listBest.insert(0, best)

    def getBest(self):
        return self.listBest
