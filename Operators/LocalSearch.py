__author__ = 'Miguel A. Marcos'

from Genome.GA1DArrayGenome import GA1DArrayGenome
from Algorithm.Algorithm import Algorithm
from Commons.error import inputParameterError

import random
# from copy import deepcopy as copy
# Pickle import
try:
    import cPickle as pickle
except:
    import pickle

class LocalSearch(object):
    def __init__(self):
        pass

    def __call__(self, g, fitBest, SR, evalsRemaining, alg):
        pass

class MTS_LS1(LocalSearch):
    def __init__(self, adjustFail=0.5, adjustMin=0.4, moveLeft=1.0, moveRight=0.5):
        self.bonus1 = 10
        self.bonus2 = 1

        self.MTSAdjustFailed = adjustFail
        self.MTSAdjustMin = adjustMin
        self.MTSMoveLeft = moveLeft
        self.MTSMoveRight = moveRight

        super(MTS_LS1, self).__init__()

    def __call__(self, gen, fitBest, SR, evalsRemaining, alg):
        if not isinstance(gen, GA1DArrayGenome):
            raise inputParameterError(str(type(gen)) + " instead of Genome")
        if not isinstance(fitBest, float):
            raise inputParameterError(str(type(fitBest)) + " instead of float")
        if not isinstance(SR, list):
            raise inputParameterError(str(type(SR)) + " instead of list")
        if not isinstance(evalsRemaining, int):
            raise inputParameterError(str(type(evalsRemaining)) + " instead of int")
        if not isinstance(alg, Algorithm):
            raise inputParameterError(str(type(alg)) + " instead of Algorithm")

        sz = gen.size()
        SRcopy = pickle.loads(pickle.dumps(SR, -1))
        SR = SR[0]
        grade = 0
        fit_inc_acum = 0.0
        evals = 0
        improvements = 0

        # If gen was not improved during last ite, update SR
        if gen.getImprove() == 0 and gen.getStep() == 0:
            SR *= self.MTSAdjustFailed
            SRcopy[0] = SR
            if SR < 1e-14:
                SR = (gen.getAllelle(0)[1] - gen.getAllelle(0)[0]) * self.MTSAdjustMin
            gen.setSR(SR)

        # Mark solution as not improved
        if gen.getLastIterPos() == 0 and gen.getStep() == 0:
            gen.setImprove(0)

        allele_min = gen.getAllelle(0)[0]
        allele_max = gen.getAllelle(0)[1]

        # Boolean value which says if the LS was interrupted in the middle of
        # an iteration in the second step of one dimension
        ignoreFirstStep = True if gen.getStep() == 1 else False
        for i in range(gen.getLastIterPos(), sz):
            if evals == evalsRemaining:
                gen.setLastIterPos(i)
                gen.setStep(0)
                return gen, grade, evals, fit_inc_acum, improvements, SRcopy
            old_gen = gen.getArray()[i]
            old_sco = gen.getScore()
            new_sco = None

            isGenAlleleMin = old_gen == allele_min

            # Only enter this branch if the search was not interrupted during the las call
            if not isGenAlleleMin and not ignoreFirstStep:
                new_gen = old_gen - (self.MTSMoveLeft * SR)

                if new_gen < allele_min:
                    new_gen = allele_min
                if new_gen > allele_max:
                    new_gen = allele_max

                gen.gene(i, new_gen)
                new_sco = alg.evaluateGenome(gen)
                gen.setScore(new_sco)

                evals += 1
                # If the probles is MAX new score have to be better
                if alg.getOptCriterion() == "MAX":
                    if new_sco > old_sco:
                        fit_inc_acum += abs((new_sco - old_sco) / old_sco)
                        improvements += 1
                    if new_sco > fitBest:
                        grade += self.bonus1
                        fitBest = new_sco
                else:
                    if new_sco < old_sco:
                        fit_inc_acum += abs((new_sco - old_sco) / old_sco)
                        improvements += 1
                    if new_sco < fitBest:
                        grade += self.bonus1
                        fitBest = new_sco

            if not isGenAlleleMin and old_sco == new_sco and not ignoreFirstStep:
                gen.gene(i, old_gen)
                gen.setScore(old_sco)
            else:
                if new_sco is None:
                    worst = True
                else:
                    if alg.getOptCriterion() == "MAX":
                        worst = old_sco > new_sco
                    else:
                        worst = new_sco > old_sco

                if isGenAlleleMin or ignoreFirstStep or worst:
                    gen.gene(i, old_gen)
                    gen.setScore(old_sco)
                    if ignoreFirstStep:
                        gen.setStep(0)
                        ignoreFirstStep = False
                    if evals == evalsRemaining:
                        gen.setStep(1)
                        gen.setLastIterPos(i)
                        return gen, grade, evals, fit_inc_acum, improvements, SRcopy
                    if old_gen != allele_max:
                        new_gen = old_gen + (self.MTSMoveRight * SR)

                        if new_gen > allele_max:
                            new_gen = allele_max
                        if new_gen < allele_min:
                            new_gen = allele_min

                        # Assert(!isnan(new_gene)

                        gen.gene(i, new_gen)
                        new_sco = alg.evaluateGenome(gen)
                        gen.setScore(new_sco)

                        evals += 1

                        fit_inc_acum += abs((new_sco - old_sco) / old_sco)

                        if alg.getOptCriterion() == "MAX":
                            if new_sco > old_sco:
                                improvements += 1
                                grade += self.bonus2  # WHY: line 275 RealOPs
                                gen.setImprove(1)
                            else:
                                # If new_gen is worst
                                gen.gene(i, old_gen)
                                gen.setScore(old_sco)
                            if new_sco > fitBest:
                                grade += self.bonus1
                                fitBest = new_sco
                        else:
                            if new_sco < old_sco:
                                improvements += 1
                                grade += self.bonus2  # WHY: line 275 RealOPs
                                gen.setImprove(1)
                            else:
                                # If new_gen is worst
                                gen.gene(i, old_gen)
                                gen.setScore(old_sco)
                            if new_sco < fitBest:
                                grade += self.bonus1
                                fitBest = new_sco
                else:
                    grade += self.bonus2
                    gen.setImprove(1)
        gen.setLastIterPos(0)
        gen.setStep(0)

        # return grade
        return gen, grade, evals, fit_inc_acum, improvements, SRcopy

class RandomGreedyLS(LocalSearch):
    def __init__(self):
        super(RandomGreedyLS, self).__init__()

    def __call__(self, gen, fitBest, SR, evalsRemaining, alg):
        if not isinstance(gen, GA1DArrayGenome):
            raise inputParameterError(str(type(gen)) + " instead of Genome")
        if not isinstance(fitBest, float):
            raise inputParameterError(str(type(fitBest)) + " instead of float")
        if not isinstance(SR, float):
            raise inputParameterError(str(type(SR)) + " instead of float")
        if not isinstance(evalsRemaining, int):
            raise inputParameterError(str(type(evalsRemaining)) + " instead of int")
        if not isinstance(alg, Algorithm):
            raise inputParameterError(str(type(alg)) + " instead of Algorithm")

        size = gen.size()
        fit_inc_acum = 0.0

        nEvals = 0
        improvements = 0

        for x in range(nEvals, evalsRemaining):
            ranPos = random.randint(0, size-1)
            tmpAllelle = gen.getAllelle()
            if isinstance(tmpAllelle, tuple):
                allelle_min = tmpAllelle[0]
                allelle_max = tmpAllelle[1]
            else:
                allelle_min = tmpAllelle[ranPos][0]
                allelle_max = tmpAllelle[ranPos][1]

            oldArray = gen.getArray()
            newArray = pickle.loads(pickle.dumps(oldArray -1))
            oldValue = oldArray[ranPos]

            newValue = random.uniform(allelle_min, allelle_max)
            newArray[ranPos] = newValue

            alg.evaluateGenome(gen)
            oldScore = gen.getScore()
            gen.setArray(newArray)
            alg.evaluateGenome(gen)
            newScore = gen.getScore()
            fit_inc_acum = 0.0  # In C code is computeFitnessIncrement a method of genome

            if oldScore > newScore:
                gen.setArray(oldArray)
            else:
                improvements += 1
            nEvals += 1

        return gen, fit_inc_acum, nEvals, fit_inc_acum, improvements


