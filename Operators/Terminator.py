__author__ = 'Miguel A Marcos Perez'

from Genome.Genome import Genome

from Commons.error import inputParameterError


class Terminator(object):
    # This class have metod done that return true if the GA must stop
    def done(self, param):
        pass


class TerminateUponConvergence(Terminator):
    # If convergence is minor to pConvergence returns true
    #   Convergence is the distance between all members of a Population
    #   If the conv is near 0 the GA can be ended
    def __init__(self, pConvergence, criterion):
        self.pConvergence = pConvergence  # Distance between members to stop
        self.nconv = 0  # Pointer of listConvergece
        self.Nconv = 10  # Number of gens to save
        self.listConvergence = []
        self.criterion = criterion

    # Evaluate the input pop
    def done(self, best):
        if not isinstance(best, Genome):
            raise inputParameterError("Parametro: Input type is " + str(type(best)) + " instead of Genome")
        # If is not inic is before first round
        if best.getScore() == 0.0:
            return False

        self.__addConv(best)

        if len(self.listConvergence) <= 1:
            return False

        # Now we have to increase nconv by 1
        self.nconv += 1

        # Firs we calculate the new conv
        conv = self.__calculateConvergence()
        #print(conv)

        if self.criterion == "MIN":
            if conv == 0 or conv > self.pConvergence:
                return False
            else:
                return True
        else:
            if conv < self.pConvergence:
                return False
            else:
                return True

    def __addConv(self, gen):
        if not isinstance(gen, Genome):
            raise inputParameterError("Parametro: Input type is " + str(type(gen)) + " instead of Genome")

        self.listConvergence.insert(0, gen.getScore())
        if len(self.listConvergence) == 11:
            self.listConvergence.pop(10)

    # To calculate conv we need to div past best gen with actual best gen
    def __calculateConvergence(self):
        conv = 0
        if not len(self.listConvergence) <= 1:  # If isnt the first round
            conv = self.listConvergence[1] / self.listConvergence[0]

        return conv

class terminateUponGeneration(Terminator):
    def __init__(self, maxGens, criterion):
        self.maxGens = maxGens  # Distance between members to stop
        self.nGen = 0  # Gen number
        self.criterion = criterion

    def done(self, totalEvaluations):
        # This method is called one time in each gen
        return totalEvaluations >= self.maxGens
