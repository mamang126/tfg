__author__ = 'Miguel A. Marcos Perez'

from Genome.GA1DArrayGenome import GA1DArrayGenome
from Commons.error import inputParameterError
from Commons.flip_coin import flip_coin
import random


class Mutator(object):

    def __init__(self):
        pass

    def __call__(self):
        pass

    def getPMut(self):
        pass

class FlipMutator(Mutator):

    def __init__(self, pMut):
        # We create the object with the input params
        if not isinstance(pMut, float) and not isinstance(pMut, int):
            raise inputParameterError("Params: Input type is " + str(type(pMut)) + " instead of float/int")
        self.pMut = pMut

    def __call__(self, genIni):

        if not isinstance(genIni, GA1DArrayGenome):
            raise inputParameterError("Params: Input type is " + str(type(genIni)) + " instead of Genome")

        arrayIni = genIni.getArray()
        arrayFin = []
        done = 0

        for ide, item in enumerate(arrayIni):
                if flip_coin(self.pMut):
                    done += 1
                    # Obtain allelle of i
                    rangeV = genIni.getAllelle(ide)
                    # If pMut is true, make mutation
                    if rangeV[1] == float("inf"):
                        rangeV = (rangeV[0], 1000)  # If allele is inf, set as maxINT
                    if genIni.getArrayType() == "int":
                        item = random.randrange(rangeV[0], rangeV[1])
                    elif genIni.getArrayType() == "float":
                        item = random.uniform(rangeV[0], rangeV[1])
                arrayFin.append(item)

        # We have already SONS, now we'll return it
        genFin = GA1DArrayGenome(arrayFin, genIni.getAllelle(), genIni.getArrayType())

        return genFin, done

    def getPMut(self):
        return self.pMut
