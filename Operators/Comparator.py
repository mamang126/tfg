__author__ = 'Miguel A Marcos'

from Genome.GA1DArrayGenome import GA1DArrayGenome
from Commons.error import inputParameterError

from cmath import sqrt

class Comparator(object):
    def __call__(self, gen1, gen2):
        if not(isinstance(gen1, GA1DArrayGenome)):
                raise inputParameterError("Params: Input type is " + str(type(gen1)) + " instead of Genome")
        if not(isinstance(gen2, GA1DArrayGenome)):
                raise inputParameterError("Params: Input type is " + str(type(gen2)) + " instead of Genome")

class RealEuclideanComparator(Comparator):
    def __call__(self, gen1, gen2):
        super().__call__(gen1, gen2)

        tam = gen1.size()
        if tam != gen2.size():
            return None
        if gen1.size() == 0:
            return 0

        result = 0.0

        for i in range(0, tam):
            xn = gen1.gene(i)
            yn = gen2.gene(i)
            tmp = xn - yn
            result += tmp * tmp

        return sqrt(result)
