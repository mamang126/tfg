__author__ = 'Miguel A. Marcos Perez'

# from Genome.Genome import Genome
from Genome.GA1DArrayGenome import GA1DArrayGenome
from Commons.error import inputParameterError, arraySameLenError
from random import randint, uniform
# from copy import deepcopy as copy

class Crossover(object):
    def __init__(self, pCross):
        self.pCros = pCross

    def __call__(self, momGen, dadGen, sonsNum):
        pass

    def getPCross(self):
        return self.pCros

class OnePointCrossover(Crossover):

    def __call__(self, momGen, dadGen, sonsNum=2):

        if not isinstance(momGen, GA1DArrayGenome):
            raise inputParameterError("Param: Input type is " + str(type(momGen)) + " instead of Genome")
        elif not isinstance(dadGen, GA1DArrayGenome):
            raise inputParameterError("Param: Input type is " + str(type(dadGen)) + " instead of Genome")
        elif sonsNum > 2 or sonsNum < 1:
            raise inputParameterError("Input value out of range 1, 2")

        momArray = momGen.getArray()
        dadArray = dadGen.getArray()

        # If array length is not equals -> Error
        if len(momArray) != len(dadArray):
            raise arraySameLenError("Param: mom len is " + str(len(momArray)) + ", father len is " + str(len(dadArray)))

        # Here we calculate a random number between 0 to len-1 for both parents
        randNum = randint(0, len(dadArray))

        sonArray1 = momArray[0:randNum] + dadArray[randNum:]
        son1 = GA1DArrayGenome(sonArray1, dadGen.getAllelle(), dadGen.getArrayType())
        if sonsNum == 2:
            sonArray2 = dadArray[0:randNum] + momArray[randNum:]
            son2 = GA1DArrayGenome(sonArray2, momGen.getAllelle(), momGen.getArrayType())
            return son1, son2
        return son1  # Si no retorna antes quiere decir que tiene un hijo

class BinomialCrossover(Crossover):

    def __call__(self, momGen, dadGen, sonsNum=2):
        if not isinstance(momGen, GA1DArrayGenome):
            raise inputParameterError("Param: Input type is " + str(type(momGen)) + " instead of Genome")
        elif not isinstance(dadGen, GA1DArrayGenome):
            raise inputParameterError("Param: Input type is " + str(type(dadGen)) + " instead of Genome")
        elif sonsNum > 2 or sonsNum < 1:
            raise inputParameterError("Input value out of range 1, 2")

        dim = momGen.size()
        if dim != dadGen.size():
            raise arraySameLenError("Param: mom len is " + str(momGen.size()) + ", father len is " + str(dadGen.size()))
        dadArray = dadGen.getArray()
        momArray = momGen.getArray()
        irand = randint(0, dim)
        finalArray = []

        for j in range(dim):
            tmprand = uniform(0.0, 1.0)

            if tmprand < self.pCros or j == irand:
                finalArray.append(dadArray[j])
            else:
                finalArray.append(momArray[j])

        return GA1DArrayGenome(finalArray, dadGen.getAllelle(), dadGen.getArrayType())

