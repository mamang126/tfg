__author__ = 'Miguel A. Marcos Perez'

from copy import deepcopy as copy

# Definition of Genome abstract class
#       GA1DArrayGenome

class Genome(object):

    # Constructor class
    def __init__(self, score=0.0):
        # Input generic parameter
        self.__score = score
        self.__evaluated = False
        self.__techId = None
        self.__mustQuality = False

    def mustComputeQuality(self, state=None):
        if state is None:
            return self.__mustQuality
        else:
            self.__mustQuality = state

    def getTechniqueId(self):
        return self.__techId

    def setTechniqueId(self, idTech):
        self.__techId = idTech

    def setScore(self, newScore):
        self.__score = newScore
        self.__evaluated = True

    def getScore(self):
        return copy(self.__score)

    def getArray(self):
        pass

    def evaluated(self):
        return self.__evaluated

    def copy(self):
        return copy(self)

    def __str__(self):
        pass