from __future__ import absolute_import
# This line is for retro compatibility between 3.4 and 2.7
__author__ = 'Miguel A. Marcos Perez'

from Genome.Genome import Genome as Genome
from Commons.error import inputParameterError

# from copy import deepcopy as copy
# Pickle import
try:
    import cPickle as pickle
except:
    import pickle

# Child of Genome, this is a Container class that have inside Real numbers

class GA1DArrayGenome(Genome):

    def __init__(self, inputList, range=(0, float("inf")), typeOf="int"):
        if not(isinstance(inputList, list)):
            raise inputParameterError("Params: Input type is " + str(type(inputList)) + " instead of list")
        if range is not None:
            if isinstance(range, list):
                for item in range:
                    if not isinstance(item, tuple):
                        raise inputParameterError("Parametro: Input type is " + str(type(item)) + " instead of tuple")
                    tmpRange = range[0]
            else:
                if not isinstance(range, tuple):
                    raise inputParameterError("Parametro: Input type is " + str(type(range)) + " instead of list or tuple")
                tmpRange = range

        # TODO If empty list?

        # Ill call it array to keep variable names
        self.__array = inputList

        # Type of array setted here
        self.__typeOf = typeOf

        # List of tuples or tuple
        #   Used math.inf to represent inf
        self.__Range = range

        # MOS variables
        self.__SR = (tmpRange[1] - tmpRange[0]) / 2
        self.__improve = 1
        self.__lastIterPos = 0
        self.__step = 0

        super(GA1DArrayGenome, self).__init__()

    def __eq__(self, other):
        if isinstance(other, GA1DArrayGenome):
            return self.__array == other.getArray() and self.getAllelle() == other.getAllelle()
        else:
            return False

    def __str__(self):
        return str(self.getArray())

    def getAllelle(self, i=None):
        if i is None:
            return self.__Range
        if isinstance(self.__Range, tuple):
            return self.__Range
        else:
            return self.__Range[i]

    def getArray(self):
        return pickle.loads(pickle.dumps(self.__array, -1))

    def setArray(self, newArray):
        if not(isinstance(newArray, list)):
            raise inputParameterError("Params: Input type is " + str(type(newArray)) + " instead of list")
        self.__array = newArray

    def getArrayType(self):
        return self.__typeOf

    def setRange(self, newRange):
        self.__Range = newRange

    def setSR(self, SR):
        self.__SR = SR

    def getSR(self):
        return self.__SR

    def setImprove(self, imp):
        self.__improve = imp

    def getImprove(self):
        return self.__improve

    def setLastIterPos(self, iter):
        self.__lastIterPos = iter

    def getLastIterPos(self):
        return self.__lastIterPos

    def setStep(self, step):
        self.__step = step

    def getStep(self):
        return self.__step

    def gene(self, pos, value):
        if not(isinstance(pos, int)):
            raise inputParameterError("Params: Input type is " + str(type(pos)) + " instead of int")

        self.__array[pos] = value

    def size(self):
        return len(self.__array)

    def copy(self):
        return pickle.loads(pickle.dumps(self, -1))

    def __str__(self):
        return str(self.__array)

    # Methods for MOS

