from __future__ import absolute_import

__author__ = 'Miguel A Marcos'

from Commons.error import inputParameterError
from Genome.GA1DArrayGenome import GA1DArrayGenome
from Genealogy.GenealogyNode import GenealogyNode


class Genealogy(object):
    __instance = None
    isInit = False

    def __new__(cls):
        if Genealogy.__instance is None:
            Genealogy.__instance = object.__new__(cls)
        return Genealogy.__instance

    def __init__(self):
        if not Genealogy.isInit:
            self.gen = 0
            self.root = [[]]
            self.isNecesary = False
            Genealogy.isInit = True

    def setNecesary(self, nec):
        if not nec:
            del self.root
        self.isNecesary = nec

    def getNecesary(self):
        return self.isNecesary

    def initRoot(self, gens):
        if not self.isNecesary:
            return
        if not (isinstance(gens, list)):
            raise inputParameterError("Params: Input type is " + str(type(gens)) + " instead of list")
        for gen in gens:
            if not (isinstance(gen, GA1DArrayGenome)):
                raise inputParameterError("Params: Input type is " + str(type(gen)) + " instead of Genome")
            node = GenealogyNode(gen)
            self.root[0].append(node)
        self.gen += 1
        self.root.append([])  # Next generation

    def setGeneration(self, gen):
        self.gen = gen

    def getGeneration(self):
        return self.gen

    def newGeneration(self):
        self.gen += 1
        if not self.isNecesary:
            return
        self.root.append([])

    def getSon(self, gen):
        if not self.isNecesary:
            return
        if not (isinstance(gen, GA1DArrayGenome)):
            raise inputParameterError("Params: Input type is " + str(type(gen)) + " instead of Genome")
        genId = id(gen)
        sons = []

        for generations in self.root:
            for gens in generations:
                parents = gens.getParentsId()
                if parents is None:
                    continue

                for par in parents:
                    if par == genId:
                        sons.append(gens.getId())
        return sons

    def getFitnessIncrementFromGen(self, gen):
        if not self.isNecesary:
            return
        if not (isinstance(gen, GA1DArrayGenome)):
            raise inputParameterError("Params: Input type is " + str(type(gen)) + " instead of Genome")

        node = self.getNodeById(id(gen))
        if node is None: return gen.getScore()
        nodeParents = node.parents
        return max(node.score - nodeParents[0].getScore(), node.score - nodeParents[1].getScore())

    def addNode(self, gen, parents, tech=None):
        if not self.isNecesary:
            return
        parentsID = []
        for gens in parents:
            parentsID.append(id(gens))

        node = GenealogyNode(gen, parentsID, tech)
        self.root[self.gen].append(node)

    def getNodeById(self, idGenome):
        if not self.isNecesary:
            return
        for generation in self.root:
            for node in generation:
                if node.getId() == idGenome:
                    return node
        return None

    def getGenerationNodes(self, i):
        if not self.isNecesary:
            return
        return self.root[i]
