__author__ = 'Miguel A Marcos Perez'

from Commons.error import inputParameterError
from Genome.GA1DArrayGenome import GA1DArrayGenome

class GenealogyNode(object):

    def __init__(self, gen, parents=None, tech=None):
        if not(isinstance(gen, GA1DArrayGenome)):
            raise inputParameterError("Params: Input type is " + str(type(gen)) + " instead of Genome")

        self.score = gen.getScore()
        self.gen = gen
        self.id = id(gen)
        self.parents = parents
        self.tech = tech

    def getId(self):
        return self.id

    def getParentsId(self):
        return self.parents

    def getScore(self):
        return self.score

    def getTech(self):
        return self.tech






