from __future__ import absolute_import
# This line is for retro compatibility between 3.4 and 2.7

__author__ = 'Miguel A Marcos Perez'

# Imports
from Genealogy.Genealogy import Genealogy

from Algorithm.Algorithm import Algorithm

from Population.Population import Population

from Genome.Genome import Genome
from Genome.GA1DArrayGenome import GA1DArrayGenome

from Commons.error import inputParameterError
from Commons import flip_coin

from Operators.Crossover import Crossover
from Operators.Mutator import Mutator
from Operators.Selector import Selector
from Operators.Elitism import Elitism
from Operators.Terminator import Terminator
from Operators.Stats import Stats
from Problems.Problem import Problem

from operator import attrgetter


class GA(Algorithm):

    def __init__(self, ID,  Input, mutator, crossover, selector, problem, eli, ter, sta, params={}):
        if not isinstance(params, dict):
            raise inputParameterError(str(type(params)) + " instead of dict")
        if not isinstance(mutator, Mutator):
            raise inputParameterError(str(type(mutator)) + " instead of Mutator")
        if not isinstance(crossover, Crossover):
            raise inputParameterError(str(type(crossover)) + " instead of Crossover")
        if not isinstance(selector, Selector):
            raise inputParameterError(str(type(selector)) + " instead of Selector")
        if not isinstance(problem, Problem):
            raise inputParameterError(str(type(problem)) + " instead of Problem")
        if not isinstance(eli, Elitism):
            raise inputParameterError(str(type(eli)) + " instead of Elitism")
        if not isinstance(sta, Stats):
            raise inputParameterError(str(type(sta)) + " instead of Stats")
        if not isinstance(ter, Terminator):
            raise inputParameterError(str(type(ter)) + " instead of Terminator")

        # Input parameters contain mutator and crossover, now we have to asign it to our class variables
        self.mutator = mutator
        self.crossover = crossover
        self.selector = selector
        self.problem = problem
        self.elitism = eli
        self.terminator = ter
        self.stat = sta

        self.ID = ID

        self.totalEvals = 0

        super(GA, self).__init__(Input, params)

    # @profile
    def step(self, maxEvals, pop=None):
        if pop is None:
            pop = self.__pop
        popSize = pop.size()
        if not isinstance(pop, Population):
            raise inputParameterError(str(type(pop)) + " instead of Population")
        if not isinstance(maxEvals, int):
            raise inputParameterError(str(type(maxEvals)) + " instead of int")

        # Looping pop until maxEvals is done
        evals = 0
        conv = 0
        while evals < maxEvals:
            evalsRemaining = maxEvals - evals
            maxEvalsRemaining = popSize if evalsRemaining >= popSize else evalsRemaining
            (nEvals, auxpop, conv) = self.__offspring(maxEvalsRemaining, pop)
            evals += nEvals
            pop = auxpop
            # One more Gen if is a complete gen
            self.stat.addEvals(nEvals)
            if nEvals == popSize:
                self.stat.addNGen(1)
                genealogy = Genealogy()
                genealogy.newGeneration()

        return evals, pop, conv

    # @profile
    def __offspring(self, maxEvals, pop):
        if not isinstance(maxEvals, int):
            raise inputParameterError(str(type(maxEvals)) + " instead of int")
        elif not isinstance(pop, Population):
            raise inputParameterError(str(type(pop)) + " instead of Population")

        # offspring_internal is called inside offspring and returns 2 new members for the pop
        nEvals = 0
        conv = 0
        auxList = []

        # If tam pob % 2 is equals to 0 we dont have to make another son
        par = True
        if maxEvals % 2 != 0:
            par = False

        while nEvals <= maxEvals - 2:

            # Selector is call to get two parents
            dad = self.selector(pop)
            mom = self.selector(pop)

            # Here offspring_internal is called with 2 ind of the pop
            (son1, son2) = self.__offspring_internal(dad, mom, 2)

            # Genealogy
            genealogy = Genealogy()
            genealogy.addNode(son1, [dad, mom], self.ID)
            genealogy.addNode(son2, [dad, mom], self.ID)

            son1.setTechniqueId(self.ID)
            son2.setTechniqueId(self.ID)

            # We have to generate the new pop list
            auxList.append(son1)
            auxList.append(son2)

            nEvals += 2

        if not par:
            dad = self.selector(pop)
            mom = self.selector(pop)
            son = self.__offspring_internal(dad, mom, 1)
            auxList.append(son)
            nEvals += 1

        auxpop = Population(auxList)

        # We have to update scores, so we have to call objetive wh the population
        self.evaluatePopulation(auxpop)
        self.evaluatePopulation(pop)

        # Now we have to call elitism recombinator
        finalPop = self.elitism(auxpop, pop, self.problem.optCriterion())

        return nEvals, finalPop, conv

    # @profile
    def __offspring_internal(self, dad, mom, nSons):
        if not isinstance(dad, Genome):
            raise inputParameterError("Parametro: Input type is " + str(type(dad)) + " instead of Genome")
        elif not isinstance(mom, Genome):
            raise inputParameterError("Parametro: Input type is " + str(type(mom)) + " instead of Genome")
        elif not isinstance(nSons, int):
            raise inputParameterError("Parametro: Input type is " + str(type(nSons)) + " instead of int")
        if nSons != 1 and nSons != 2:
            raise inputParameterError("Parametro: Input type is " + str(nSons) + " instead of 1 or 2")

        # Instanciate sons
        son1 = GA1DArrayGenome([], dad.getAllelle(), dad.getArrayType())
        son2 = GA1DArrayGenome([], mom.getAllelle(), mom.getArrayType())

        # Crossover is done if flipcoin with pCross is true
        pCross = self.crossover.getPCross()
        if flip_coin.flip_coin(pCross):
            self.stat.addCross(1)
            if nSons == 1:
                son1 = self.crossover(dad, mom, 1)
            else:
                son1, son2 = self.crossover(dad, mom)
        else:
            # In case of pCross is not true, clone parents into sons
            son1 = dad
            son2 = mom

        # Call mutation
        # If nSons is equals to 1 only return 1 son.
        (son1, done) = self.mutator(son1)
        self.stat.addMut(done)
        if nSons == 1:
            return son1

        # If nSons equals to 2, we return 2 childs
        (son2, done) = self.mutator(son2)
        self.stat.addMut(done)

        return son1, son2

    def done(self, pop, totalEvaluations):
        tmpBest = self.best(pop)
        self.stat.addBest(tmpBest._Genome__score)
        # We have to include best gen of actual gen into terminator class
        return self.terminator.done(totalEvaluations)

    def getEvals(self):
        return self.totalEvals

    def setEvals(self, value):
        if not isinstance(value, int):
            raise inputParameterError("Parametro: Input type is " + str(type(value)) + " instead of int")
        self.totalEvals = value

    def best(self, pop=False):
        if not pop:
            pop = self._Algorithm__pop
        if not isinstance(pop, Population):
            raise inputParameterError("Parametro: pop type is " + str(type(pop)) + " instead of Population")
        return max(pop.get(), key=attrgetter('_Genome__score'))




