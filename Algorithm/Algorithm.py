__author__ = 'Miguel A. Marcos Perez'

from Genome.Genome import Genome
from Population.Population import Population
from Commons.error import inputParameterError
# from copy import deepcopy as copy
from operator import attrgetter
# Pickle import
try:
    import cPickle as pickle
except:
    import pickle

class Algorithm(object):
    # Algorithm variable class:
    #   Para saber que variables utilizar voy a analizar el codigo de GAGeneticAlgorithm
    #       en caso de necesitar o eliminar realizare edits...
    #   To know about what variables im going to use ill analice the GAGeneticAlgorithm code.
    #       if i need to mod the actual code i will make edits

    # Algorithm class constructor:
    #   Input params:
    def __init__(self, Input, params=[]):
        if isinstance(Input, Genome):
            # Build an algorithm by a Genome
            self.__pop = Population(Input)

        elif isinstance(Input, Population):
            # Buil an algorithm by a Population
            self.__pop = Input

        else:
            raise inputParameterError(str(type(Input)) + " instead of Genome/Population")

        # Now instanciate variables by Params
        self.__params = params
        self.__conv = 0

    # Algorithm class methods

    # Initialize method:
    def initialize(self):
        pass

    # Run method:
    #   This method will call initialize method and will evolve the Population of the algorithm
    def run(self, maxEvals=None):
        self.initialize()
        nEvals, pop = self.evolve(maxEvals, self.__pop)
        return nEvals, pop

    # Evolve method:
    # This will do a loop that wont stop until method Done dont return true
    # @profile
    def evolve(self, maxEvals=None, pop=None):
        if pop is None:
            pop = self.__pop

        if not isinstance(maxEvals, int):
            if maxEvals is None:
                maxEvals = pop.size()
            else:
                raise inputParameterError(str(type(maxEvals)) + " instead of int")

        totalEvaluations = 0

        while not self.done(pop, totalEvaluations) and totalEvaluations < maxEvals:
            # This is a exit condition cheat
            # if nEvals >= maxEvals:
            #    break

            (nEvals, auxPop, conv) = self.step(maxEvals, pop)
            pop = auxPop
            self.__pop = pop
            totalEvaluations += nEvals

        return totalEvaluations, pop

    # Best method:
    #   Output parameters:
    #       Returns the best genome in the Population,
    #       this Population is a class variable instanciated in the init method

    # Stats method:
    def stats(self):
        pass

    # Population method:
    #   Output parameter:
    #       Return the Population class variable
    def population(self):
        return self.__pop

    # Step method:
    #   This method make an iteritation to the Population that is inside the algorithm
    #   Output parameters:
    #       This will modify class Population without return it
    def step(self, maxEvals, pop):
        pass

    # Done method:
    #   This method will implement the function passed in the instantiation named Stop Condition
    def done(self, pop, totalEvaluations):
        pass

    def copy(self):
        return pickle.loads(pickle.dumps(self, -1))

    def evaluatePopulation(self, pop):
        auxPop = pop.get()
        for ind in auxPop:
            var = self.problem.objective(ind)
            ind.setScore(var)
        pop.set(auxPop)

    def evaluateGenome(self, gen):
        return self.problem.objective(gen)

    def getOptCriterion(self):
        return self.problem.optCriterion()

    def sort(self, pop):
        if not isinstance(pop, Population):
            raise inputParameterError("Parametro: pop type is " + str(type(pop)) + " instead of Population")
        popLst = pop.get()

        if self.problem.optCriterion() == "MAX":
            newLst = sorted(popLst, key=attrgetter('_Genome__score'), reverse=False)
        else:
            newLst = sorted(popLst, key=attrgetter('_Genome__score'), reverse=True)
        return Population(newLst)

    def best(self, pop=False):
        if not pop:
            pop = self._Algorithm__pop
        if not isinstance(pop, Population):
            raise inputParameterError("Parametro: pop type is " + str(type(pop)) + " instead of Population")

        if self.problem.optCriterion() == "MAX":
            return max(pop.get(), key=attrgetter('_Genome__score'))
        else:
            return min(pop.get(), key=attrgetter('_Genome__score'))

    def getStats(self):
        return self.stat
