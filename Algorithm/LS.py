from __future__ import absolute_import
# This line is for retro compatibility between 3.4 and 2.7
__author__ = 'Miguel A Marcos'

# Imports
from Algorithm.Algorithm import Algorithm

from Genealogy.Genealogy import Genealogy

from Population.Population import Population

from Commons.error import inputParameterError

from Operators.Elitism import LSElitism
from Operators.Terminator import Terminator
from Operators.Stats import Stats
from Problems.Problem import Problem

from operator import attrgetter


class LS(Algorithm):
    def __init__(self, ID, Input, ls, problem, ter, sta, params={}):
        if not isinstance(params, dict):
            raise inputParameterError(str(type(params)) + " instead of dict")
        if not isinstance(problem, Problem):
            raise inputParameterError(str(type(problem)) + " instead of Problem")
        if not isinstance(sta, Stats):
            raise inputParameterError(str(type(sta)) + " instead of Stats")
        if not isinstance(ter, Terminator):
            raise inputParameterError(str(type(ter)) + " instead of Terminator")

        # Input parameters contain mutator and crossover, now we have to asign it to our class variables
        # In this case we have to use a predefined selector
        self.problem = problem
        self.elitism = LSElitism
        self.terminator = ter
        self.stat = sta
        self.ls = ls
        self.ID = ID

        tmpPop = Input.get()
        self.__SR = []

        gen = tmpPop[0]
        allele = gen.getAllelle()
        if isinstance(allele, tuple):
            value = (allele[1] - allele[0]) / 2.0
            for val in gen.getArray():
                self.__SR.append(value)
        else:
            for alle in allele:
                self.__SR.append((alle[1] - alle[0]) / 2.0)

        self.__CurrentGenome = None

        self.totalEvals = 0

        super(LS, self).__init__(Input, params)

    def step(self, maxEvals, pop):
        return self.__offspring(maxEvals, pop)

    # @profile
    def __offspring(self, maxEvals, pop):
        if not isinstance(maxEvals, int):
            raise inputParameterError(str(type(maxEvals)) + " instead of int")
        elif not isinstance(pop, Population):
            raise inputParameterError(str(type(pop)) + " instead of Population")

        # If is no currentGen declared we have to assign the best gen into
        if self.__CurrentGenome is None:
            self.__CurrentGenome = self.__selectBest(pop)
            # If is not init we have to update score
            if self.__CurrentGenome is None:
                self.evaluatePopulation(pop)
                self.__CurrentGenome = self.__selectBest(pop)

        g = self.__CurrentGenome.copy()

        nEvals = 0
        fit_inc_acum_total = 0.0
        fitBest = g.getScore()
        # Inic score
        if fitBest == 0.0:
            fitBest = self.evaluateGenome(g)

        conv = False

        # DO-WHILE loop
        condition = True
        while condition:
            evalsRemaining = maxEvals - nEvals
            gNew, grade, evals, fit_inc_acum, improvement, SR = self.ls(g, fitBest, self.__SR, evalsRemaining, self)

            # Genealogy
            genealogy = Genealogy()
            genealogy.addNode(gNew, [g], self.ID)

            self.__SR = SR

            # Acum values returned
            fit_inc_acum_total += fit_inc_acum
            nEvals += evals

            self.stat.addNGen(1)
            genealogy.newGeneration()

            gNew.setTechniqueId(self.ID)
            g = gNew

            # In C code is called preccisionReached of genome
            # TODO: in code C++ is called GAEDAConfig

            condition = nEvals < maxEvals #and not conv

        self.stat.addMut(nEvals)
        self.stat.addEvals(nEvals)
        self.stat.addCross(0)

        # We have to set the new individual created in the first position of the final population
        pop.set(g, 0)

        return nEvals, pop, conv

    def __selectBest(self, pop=None):
        if not pop:
            pop = self._Algorithm__pop
        if not isinstance(pop, Population):
            raise inputParameterError("Parametro: pop type is " + str(type(pop)) + " instead of Population")
        gen = max(pop.get(), key=attrgetter('_Genome__score'))
        if gen.getScore() == 0.0:
            gen = None
        return gen

    def done(self, pop, totalEvaluations):
        tmpBest = self.best(pop)
        self.stat.addBest(tmpBest._Genome__score)
        # We have to include best gen of actual gen into terminator class
        return self.terminator.done(totalEvaluations)
