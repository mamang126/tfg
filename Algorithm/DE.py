from __future__ import absolute_import

__author__ = 'Miguel A. Marcos'

# Imports
from Algorithm.Algorithm import Algorithm

from Genealogy.Genealogy import Genealogy

from Population.Population import Population

from Genome.GA1DArrayGenome import GA1DArrayGenome

from Operators.Elitism import DEElitism

from random import sample

from Commons.error import inputParameterError
from Commons import flip_coin

from Operators.Stats import Stats
from Problems.Problem import Problem

from operator import attrgetter

'''
    DE Algorithm:
        for each gen called X:
            get 3(A, B, C) gen that have to be distinct between each others and X (selectParents)
            pick a random index (0, N-1)
                give
'''


class DE(Algorithm):
    def __init__(self, ID, Input, pMut, crossover, problem, sta, ter, F, params={}):
        if not isinstance(params, dict):
            raise inputParameterError(str(type(params)) + " instead of dict")
        if not isinstance(problem, Problem):
            raise inputParameterError(str(type(problem)) + " instead of Problem")
        if not isinstance(sta, Stats):
            raise inputParameterError(str(type(sta)) + " instead of Stats")

        # Input parameters contain mutator and crossover, now we have to asign it to our class variables
        # In this case we have to use a predefined selector
        self.selector = selectParents  # Here we have to use the selectParets coded in this class
        self.problem = problem
        self.stat = sta
        self.crossover = crossover
        self.pMut = pMut
        self.elitism = DEElitism()
        self.terminator = ter
        self.ID = ID

        self.totalEvals = 0

        self._F = F

        super(DE, self).__init__(Input, params)

    # @profile
    def step(self, maxEvals, pop=None):
        if pop is None:
            pop = self.__pop
        popSize = pop.size()
        if not isinstance(pop, Population):
            raise inputParameterError(str(type(pop)) + " instead of Population")
        if not isinstance(maxEvals, int):
            raise inputParameterError(str(type(maxEvals)) + " instead of int")

        # Looping pop until maxEvals is done
        evals = 0
        conv = 0
        while evals < maxEvals:
            evalsRemaining = maxEvals - evals
            maxEvalsRemaining = popSize if evalsRemaining >= popSize else evalsRemaining
            (nEvals, auxpop, conv) = self.__offspring(maxEvalsRemaining, pop)
            evals += nEvals
            pop = auxpop

            # One more Gen
            self.stat.addEvals(nEvals)
            if nEvals == popSize:
                self.stat.addNGen(1)
                genealogy = Genealogy()
                genealogy.newGeneration()

        return evals, pop, conv

    # @profile
    def __offspring(self, maxEvals, pop):
        if not isinstance(maxEvals, int):
            raise inputParameterError(str(type(maxEvals)) + " instead of int")
        elif not isinstance(pop, Population):
            raise inputParameterError(str(type(pop)) + " instead of Population")

        # offspring_internal is called inside offspring and returns 2 new members for the pop
        nEvals = 0
        conv = 0
        auxList = []

        for ind in pop.get():
            # We have not to pass maxEvals..
            if nEvals >= maxEvals:
                # If nEvals is max Evals we have to exit
                popArray = pop.get()
                for n in range(nEvals, pop.size()):
                    auxList.append(popArray[n])
                break

            # Selector is call to get two parents
            (t1, t2, t3) = self.selector(pop, ind)

            # Here offspring_internal is called with 4 ind of the pop and return only one result
            son1 = self.__offspring_internal(ind, t1, t2, t3)

            # Genealogy
            genealogy = Genealogy()
            genealogy.addNode(son1, [ind, t1, t2, t3], self.ID)

            # We have to generate the new pop list
            if son1.getArrayType() == "int":
                newGen = []
                for ele in son1.getArray():
                    newGen.append(int(ele))
                son1.setArray(newGen)

            son1.setTechniqueId(self.ID)
            auxList.append(son1)

            nEvals += 1

        auxpop = Population(auxList)

        # We have to update scores, so we have to call objetive wh the population
        self.evaluatePopulation(auxpop)
        self.evaluatePopulation(pop)

        # Now we have to call elitism recombinator
        finalPop = self.elitism(auxpop, pop, self.problem.optCriterion())  # Is necesary?

        return nEvals, finalPop, conv

    def __offspring_internal(self, x, a, b, c):
        # We have to iterate over x and in each loop assign a uniform value to perform a mutation
        aList = a.getArray()
        bList = b.getArray()
        cList = c.getArray()

        genAux = []
        # Mutation for DE
        for i in range(0, x.size()):
            result = aList[i] + self._F * (bList[i] - cList[i])

            # Minimun allelle
            minAllele = x.getAllelle(i)[0]
            maxAllele = x.getAllelle(i)[1]
            if minAllele > result:
                result = minAllele
            # Maximum allelle
            elif maxAllele < result:
                result = maxAllele

            genAux.append(result)
            self.stat.addMut(1)

        # Now we have to calculate both scores to know which is the best
        son = GA1DArrayGenome(genAux, x.getAllelle(), x.getArrayType())

        self.stat.addCross(1)
        son = self.crossover(x, son, sonsNum=1)

        sonScore = self.problem.indObjective(son)
        dadScore = self.problem.indObjective(x)

        x.setScore(dadScore)
        son.setScore(sonScore)

        if self.problem.optCriterion() == "MAX":
            if dadScore > sonScore:
                return x
            else:
                return son
        else:
            if dadScore < sonScore:
                return x
            else:
                return son

    def done(self, pop, totalEvaluations):
        tmpBest = self.best(pop)
        self.stat.addBest(tmpBest._Genome__score)
        # We have to include best gen of actual gen into terminator class
        return self.terminator.done(totalEvaluations)


# This method will return 3 gens that are distinct between each others and input param
def selectParents(pop, ind):
    correct = False
    selection = []
    population = pop.get()
    while not correct:
        selection = sample(population, 3)
        correct = selection[0] != ind and selection[1] != ind and selection[2] != ind

    return selection[0], selection[1], selection[2]
