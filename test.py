__author__ = 'Miguel A. Marcos Perez'
# Este script sera utilizado para realizar test

# Importamos clase para testing
import unittest
import copy

from Genome.GA1DArrayGenome import GA1DArrayGenome
from Population.Population import Population
from Algorithm.GA import GA

from Operators.Mutator import FlipMutator
from Operators.Crossover import OnePointCrossover, BinomialCrossover
from Operators.Selector import TournamentSelector
from Operators.Elitism import PopElitism
from Operators.Terminator import TerminateUponConvergence, terminateUponGeneration
from Operators.Stats import GAStatistics
from Operators.Initializer import RealUniformInitializer

from Commons.flip_coin import flip_coin

from Problems.TestProblem import TestProblem

import random

'''
Inicializamos la maquina de numeros aleatorios
'''
nSeed = 101
print("Seed usada: " + str(nSeed))
random.seed(nSeed)

'''
Test para Genome
'''

class TestGenomeMethods(unittest.TestCase):

    listaGenoma = [1,2,3,4]

    def test_getScore(self):
        test = GA1DArrayGenome(self.listaGenoma)
        self.assertEqual(test.getScore(),0.0)
        test.setScore(1.0)
        self.assertEqual(test.getScore(),1.0)

    def test_getScoreAndMod(self):
        test = GA1DArrayGenome(self.listaGenoma)
        var = test.getScore()
        var = 2.0
        self.assertEqual(test.getScore(),0.0)
        test.setScore(1.0)
        self.assertEqual(test.getScore(),1.0)

'''
Test para GA1DArrayGenome
'''

class TestGA1DArrayGenomeMethods(unittest.TestCase):

    listaGenoma = [1,2,3,4]
    test = GA1DArrayGenome(listaGenoma)

    def test_getset(self):
        self.assertEqual(self.test.getArray(),self.listaGenoma)
        newArray=[4,3,2,1]
        self.test.setArray(newArray)
        self.assertEqual(self.test.getArray(),newArray)

    def test_copyAndCompare(self):
        # Copiamos un GA1D array en otra variable y modificamos la segunda. Comprobamos que la primera sigue intacta
        var1 = GA1DArrayGenome(self.listaGenoma)
        var2 = var1.copy()

        listaGenoma2 = [5,6,7,8]
        var2.setArray(listaGenoma2)

        self.assertEqual(var1.getArray(), self.listaGenoma)
        self.assertEqual(var2.getArray(), listaGenoma2)

'''
Test para Population
'''

class TestPopulationMethods(unittest.TestCase):

    listaGenoma1 = GA1DArrayGenome([1,2,3,4])
    listaGenoma2 = GA1DArrayGenome([5,6,7,8])
    listaPoblacion = [listaGenoma1, listaGenoma2]

    def test_get(self):
        test = Population(self.listaPoblacion)
        var = test.get()
        var = None

        self.assertEqual(test.get(), self.listaPoblacion)
        self.assertEqual(test.get(1),self.listaPoblacion[1])

    def test_set(self):
        test = Population(self.listaPoblacion)
        listaPoblacion2 = [self.listaGenoma1, self.listaGenoma1]

        test.set(listaPoblacion2)

        self.assertEqual(test.get(), listaPoblacion2)
        self.assertNotEqual(test.get(), self.listaPoblacion)

        # Volvemos a estado principal
        test.set(self.listaPoblacion)
        self.assertEqual(test.get(), self.listaPoblacion)

        # Modificamos un Genoma
        test.set(self.listaGenoma1, 1)
        self.assertEqual(test.get(), [self.listaGenoma1, self.listaGenoma1])

    def test_getAndSet(self):
        test = Population(self.listaPoblacion)
        var = test.get()
        var = Population([])
        self.assertEqual(test, Population(self.listaPoblacion))

'''
    Test de Algorithm
'''

class TestAlgorithmMethods(unittest.TestCase):

    gen1 = GA1DArrayGenome([1,2,3,4])
    gen2 = GA1DArrayGenome([5,6,7,8])
    gen3 = GA1DArrayGenome([9,10,11,12])
    gen4 = GA1DArrayGenome([13,14,15,16])
    gen5 = GA1DArrayGenome([17,18,19,20])
    gen6 = GA1DArrayGenome([21,22,23,24])
    gen7 = GA1DArrayGenome([25,26,27,28])
    gen8 = GA1DArrayGenome([29,30,31,32])
    gen9 = GA1DArrayGenome([33,34,35,36])
    gen10 = GA1DArrayGenome([37,38,39,40])
    gen11 = GA1DArrayGenome([41,42,43,44])
    gen12 = GA1DArrayGenome([45,46,47,48])
    gen13 = GA1DArrayGenome([49,50,51,52])

    pop = Population([gen1, gen2, gen3, gen4, gen5, gen6, gen7, gen8, gen9, gen10, gen11, gen12, gen13])

    # We have to create mutator and crossover
    mut = FlipMutator(0)
    cross = OnePointCrossover(1)
    sel = TournamentSelector()
    pro = TestProblem()
    eli = PopElitism()
    ter = terminateUponGeneration(0.9, "MAX")
    sta = GAStatistics()

    alg = GA(pop, mut, cross, sel, pro, eli, ter, sta, {})

    def test_run(self):
        self.alg.run()

    def test_funcional(self):
        nEvals, pop = self.alg.run()


class TestCrossoverMethods(unittest.TestCase):

    def test_RealBlendCrossoverrPar(self):
        random.seed(nSeed)
        listaGenoma = [1, 2, 3, 4]
        listaGenoma2 = [5, 6, 7, 8]
        genoma1 = GA1DArrayGenome(listaGenoma)
        genoma2 = GA1DArrayGenome(listaGenoma2)

        hijoEsp1 = GA1DArrayGenome([5, 6, 7, 8])
        hijoEsp2 = GA1DArrayGenome([1, 2, 3, 4])

        crossover = OnePointCrossover(1)
        hijo1, hijo2 = crossover(genoma1, genoma2)
        self.assertEqual(hijoEsp2, hijo1)
        self.assertEqual(hijoEsp1, hijo2)

    def test_RealBlendCrossoverrImpar(self):
        random.seed(nSeed)
        listaGenoma = [1, 2, 3, 4, 5]
        listaGenoma2 = [6, 7, 8, 9, 10]
        genoma1 = GA1DArrayGenome(listaGenoma)
        genoma2 = GA1DArrayGenome(listaGenoma2)

        hijoEsp1 = GA1DArrayGenome([1, 2, 3, 4, 10])
        hijoEsp2 = GA1DArrayGenome([6, 7, 8, 9, 5])

        crossover = OnePointCrossover(1)
        hijo1, hijo2 = crossover(genoma1, genoma2, 2)
        self.assertEqual(hijoEsp1, hijo1)
        self.assertEqual(hijoEsp2, hijo2)

    def test_ExponentialCrossoverr(self):
        random.seed(nSeed)
        listaGenoma = [1, 2, 3, 4, 5]
        listaGenoma2 = [6, 7, 8, 9, 10]
        genoma1 = GA1DArrayGenome(listaGenoma)
        genoma2 = GA1DArrayGenome(listaGenoma2)

        hijoEsp1 = GA1DArrayGenome([1, 2, 3, 9, 10])

        crossover = BinomialCrossover(0.5)
        hijo1 = crossover(genoma1, genoma2, 2)
        self.assertEqual(hijoEsp1, hijo1)

class TestFlipcoinmMethods(unittest.TestCase):

    def test_Flipcoin(self):
        # Reiniciamos maquina aleatoria para que este test siempre tenga mismo estado aleatorio
        random.seed(nSeed)
        pMut = 0.5
        valor = flip_coin(0.5)
        self.assertEqual(valor, False)

class TestMutatorFunction(unittest.TestCase):

    def test_Mutator(self):
        random.seed(nSeed)
        gen = GA1DArrayGenome([1,2,3,4,5,6,7,8,9,10])

        mutator = FlipMutator(0.5)
        (genFin, done) = mutator(gen)
        self.assertEqual(genFin, GA1DArrayGenome([1, 988, 3, 49, 5, 227, 7, 801, 994, 261]))

'''
Test para Elitism
'''

class TestElitismMethods(unittest.TestCase):

    listaGenoma = [1,2,3,4]
    listaGenoma2 = [5,6,7,8]
    listaGenoma3 = [15,15,15,15]
    listaGenoma4 = [30,30,30,30]
    gen1 = GA1DArrayGenome(listaGenoma)
    gen1.setScore(10)
    gen2 = GA1DArrayGenome(listaGenoma2)
    gen2.setScore(26)
    gen3 = GA1DArrayGenome(listaGenoma3)
    gen3.setScore(60)
    gen4 = GA1DArrayGenome(listaGenoma4)
    gen4.setScore(120)
    pop1 = Population([gen1, gen4])
    pop2 = Population([gen3,gen2])

    # Create the Elitism Operator
    eli = PopElitism()

    def test_call(self):
        finalPop = self.eli(self.pop1, self.pop2)
        tstPop = Population([self.gen4, self.gen3])
        self.assertEqual(finalPop, tstPop)

'''
Test para Initializer
'''

class TestInitializerMethods(unittest.TestCase):

    random.seed(nSeed)
    listaGenoma = [1,2,3,4]
    gen1 = GA1DArrayGenome(listaGenoma, (0,10))
    gen1.setScore(10)
    tstGen = GA1DArrayGenome([1.947544955341367, 9.652511070611112, 9.239764016767943, 4.67138678196974], (0,10))

    # Create the Elitism Operator
    ini = RealUniformInitializer()

    def test_call(self):
        finalGen = self.ini(self.gen1)
        self.assertEqual(finalGen, self.tstGen)

class TestFinal(unittest.TestCase):

    def test_FinalOneGen(self):
        random.seed(nSeed)
        gen = GA1DArrayGenome([1,2,3,4,5,6,7,8,9,10])

        # We have to create mutator and crossover
        mut = FlipMutator(0.2)
        cross = OnePointCrossover(1)
        sel = TournamentSelector()
        pro = TestProblem()
        eli = PopElitism()
        ter = terminateUponGeneration(2, "MAX")
        sta = GAStatistics()

        alg = GA(gen, mut, cross, sel, pro, eli, ter, sta, {})
        nEvals, popFin = alg.evolve()
        self.assertEqual(nEvals, 2)
        popTst = Population([GA1DArrayGenome([25, 632, 719, 417, 2, 168, 506, 163, 788, 148])])
        self.assertEqual(Population(gen), popFin)

    # @profile
    def test_Final20Gen(self):
        random.seed(nSeed)
        gen=[]
        for n in range(0,20):
            listaEle = random.sample(range(1, 1000), 10)
            gen.append(GA1DArrayGenome(listaEle))
        self.assertEqual(len(gen), 20)

        # We have to create mutator and crossover
        mut = FlipMutator(0.05)
        cross = OnePointCrossover(1)
        sel = TournamentSelector()
        pro = TestProblem()
        eli = PopElitism()
        ter = terminateUponGeneration(40, "MAX")
        sta = GAStatistics()

        # Creamos algoritmo
        alg = GA(Population(gen), mut, cross, sel, pro, eli, ter, sta, {})
        nEvals, popFin = alg.evolve()
        self.assertEqual(nEvals, 40)

    # @profile
    def test_Final100Gen(self):
        random.seed(nSeed)
        gen=[]
        for n in range(0,100):
            listaEle = random.sample(range(1, 1000), 10)
            gen.append(GA1DArrayGenome(listaEle))
        self.assertEqual(len(gen), 100)

        # We have to create mutator and crossover
        mut = FlipMutator(0.05)
        cross = OnePointCrossover(1)
        sel = TournamentSelector()
        pro = TestProblem()
        eli = PopElitism()
        ter = terminateUponGeneration(200, "MAX")
        sta = GAStatistics()

        # Creamos algoritmo
        alg = GA(Population(gen), mut, cross, sel, pro, eli, ter, sta, {})
        nEvals, popFin = alg.evolve()
        self.assertEqual(nEvals, 200)

class TestDebbuger(unittest.TestCase):

    # @profile
    # Comentado debido a que ya se realiza en testFinal. Este metodo es utilizado en caso de realizar line-profiling
    def test_Final10Gen(self):
        random.seed(nSeed)
        gen=[]
        for n in range(0,10):
            listaEle = random.sample(range(1,1000), 10)
            gen.append(GA1DArrayGenome(listaEle))
        self.assertEqual(len(gen), 10)
        # Creamos algoritmo
        mut = FlipMutator(0.05)
        cross = OnePointCrossover(1)
        sel = TournamentSelector()
        pro = TestProblem()
        eli = PopElitism()
        ter = terminateUponGeneration(20, "MAX")
        sta = GAStatistics()

        alg = GA(Population(gen), mut, cross, sel, pro, eli, ter, sta, {})
        nEvals, popFin = alg.evolve()
        self.assertEqual(nEvals, 20)

class TestTerminator(unittest.TestCase):

    # @profile
    # Comentado debido a que ya se realiza en testFinal. Este metodo es utilizado en caso de realizar line-profiling
    def test_TerminatorConv(self):
        random.seed(nSeed)

        term = TerminateUponConvergence(0.5, "MAX")
        pop = Population([GA1DArrayGenome([1,2,3,4])])
        val = term.done(pop.get(0))


if __name__ == '__main__':
    import sys
    if sys.argv[0] == "f" or sys.argv[0] == "F" or True:
        suite = unittest.TestLoader().loadTestsFromTestCase(TestDebbuger)
        unittest.TextTestRunner(verbosity=2).run(suite)
    else:
        # Test GA1DArrayGenome
        suite = unittest.TestLoader().loadTestsFromTestCase(TestGA1DArrayGenomeMethods)
        unittest.TextTestRunner(verbosity=2).run(suite)

        # Test Genome
        suite = unittest.TestLoader().loadTestsFromTestCase(TestGenomeMethods)
        unittest.TextTestRunner(verbosity=2).run(suite)

        # Test Mutator
        suite = unittest.TestLoader().loadTestsFromTestCase(TestMutatorFunction)
        unittest.TextTestRunner(verbosity=2).run(suite)

        # Test Population
        suite = unittest.TestLoader().loadTestsFromTestCase(TestPopulationMethods)
        unittest.TextTestRunner(verbosity=2).run(suite)

        # Test FlipCoin
        suite = unittest.TestLoader().loadTestsFromTestCase(TestFlipcoinmMethods)
        unittest.TextTestRunner(verbosity=2).run(suite)

        # Test Crossover
        suite = unittest.TestLoader().loadTestsFromTestCase(TestCrossoverMethods)
        unittest.TextTestRunner(verbosity=2).run(suite)

        # Test Algorithms
        suite = unittest.TestLoader().loadTestsFromTestCase(TestAlgorithmMethods)
        unittest.TextTestRunner(verbosity=2).run(suite)

        # Test Final
        suite = unittest.TestLoader().loadTestsFromTestCase(TestFinal)
        unittest.TextTestRunner(verbosity=2).run(suite)
