__author__ = 'miwoo'

'''
    This class is the abstract class that will be used in all problems
'''


class Problem(object):

    def __init__(self):
        pass

    def describeProblem(self):
        pass

    def optCriterion(self):
        pass

    def defineProblem(self):
        pass

    def objective(self, ind):
        pass

    def indObjective(self, ind):
        pass
