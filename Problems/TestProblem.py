__author__ = 'Miguel A. Marcos'

from .Problem import Problem
from Genome.Genome import Genome
from Commons.error import inputParameterError


class TestProblem(Problem):
    def objective(self, ind):
        if not isinstance(ind, Genome):
            raise inputParameterError("Parametro: Input type is " + str(type(ind)) + " instead of Genome")
        # Loop the population

        var = self.indObjective(ind)
        return var

    def indObjective(self, gen):
        return self.__f(gen)

    def optCriterion(self):
        return "MAX"

    def defineProblem(self):
        return "Extended f10"

    def __f(self, gen):
        indArray = gen.getArray()
        var = 0
        for gen in indArray:
            var += gen
        return var
