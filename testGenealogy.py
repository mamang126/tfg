__author__ = 'miwoo'

from Genealogy.Genealogy import Genealogy, GenealogyNode
from Genome.GA1DArrayGenome import GA1DArrayGenome
from Problems.f7 import f7

import time
# import uuid

problem = f7()
genealogy = Genealogy()

nElem = 100
nGens = 5000000
array = [1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0]

# Create 10 parents
parents = []
for i in range(0, int(nElem/2)):
    genDad = GA1DArrayGenome(array)
    genMom = GA1DArrayGenome(array)
    parents.append(genDad)
    parents.append(genMom)

genealogy.initRoot(parents)

sons = []
for j in range(0, nGens):
    for i in range(0, nElem, 2):
        genSon1 = GA1DArrayGenome(array)
        genSon1.setScore(problem.indObjective(genSon1))
        genSon2 = GA1DArrayGenome(array)
        genSon2.setScore(problem.indObjective(genSon2))

        sons.append(genSon1)
        sons.append(genSon2)
        genealogy.addNode(genSon1, [parents[i], parents[i+1]])
        genealogy.addNode(genSon2, [parents[i], parents[i+1]])

    parents = sons
    sons = []


dad = genealogy.getNodeById(id(genDad))
if dad.getId() != id(genDad):
    print("Error")

mom = genealogy.getNodeById(id(genMom))
if mom.getId() != id(genMom):
    print("Error")

print("Starting")

start_time = time.time()
sons = genealogy.getSon(genDad)
elapsed_time = time.time() - start_time

print("Time: " + str(elapsed_time))
print(sons)
print(id(genSon1))
print(id(genSon2))

genealogy = Genealogy()
print("Fin")